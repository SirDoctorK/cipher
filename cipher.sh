#!/bin/bash


########## Variables


# These variables are used for bold and normal text
bl=''
nm=''
# They are only set if stdout is a terminal (file descriptor 1 is open on a terminal)
# This is because if the output is redirected to another instance of itself or sequence,
# it will mess up the terminal when it processes text with these codes
if [ -t 1 ]; then
  bl=$(tput bold)
  nm=$(tput sgr0)
fi

HELP_TEXT="
Cipher script
Written by Stephen Kendall

Capable of deciphering and enciphering with 
  Caesar, Atbash, A1Z26, Vigenere, Beaufort, Keyword, Playfair, and Bifid.
The Vigenere implementation supports the Beaufort, Gronsfeld, and Autokey variations.
If multiple ciphers are requested, the same input string will be processed by each one separately
  unless sequential operation is selected."

# URL to use to generate text
GENURL="https://randomword.com/paragraph"

HELP_FLAGS="
Script Modifiers:
  -h, --help            show this help message and exit
  -i FILE, --input FILE
                        file containing text to process
                        if this flag is not set, it will read from standard input through redirection or keyboard input
  -g, --generate        Generate the input text from '$GENURL'
  -s, --strip, --strip-spaces
                        strip space characters from output string
  -v, --verbose         enable more detailed output (-v=info, -vv=tutorial, -vvv=step-by-step)
  --runtime             print detailed execution times (for debug)
  -m POLICY, --policy POLICY, --mismatch-policy POLICY
                        select policy for ciphers that don't operate on the contents of the input string: warn (default), continue, or skip
  -c CASE, --case CASE  convert output string to the specified case

Cipher Selectors (at least one required):
  -C, --caesar          use the Caesar cipher with a 3-character shift
  -O DIST, --offset DIST
                        use the Caesar cipher with the specified distance, inplies '-C'
  -A, --atbash          use the Atbash cipher
  -Z, --a1z26           use the A1Z26 cipher
  -V VKEY, -G VKEY, --vigenere VKEY, --gronsfeld VKEY
                        use the Vigenere cipher or a variant
  -U UKEY, --autokey UKEY
                        use the Vigenere Autokey cipher
  -B BKEY, --beaufort BKEY
                        use the Beaufort cipher
  -K KKEY, --keyword KKEY
                        use the Keyword cipher
  -P PKEY, --polybius PKEY
                        use the Polybius cipher
  -F FKEY, --playfair FKEY
                        use the Playfair cipher
  -I IKEY, --bifid IKEY
                        use the Bifid cipher
  -S SUBS, --substitute SUBS, --substitution SUBS
                        provide specific monoalphabetic substitutions to perform
  -R, --random          randomly generate and apply a substitution alphabet

Cipher Modifiers:
  -q, --seq, --sequential
                        perform the cipher operations in sequential order, where the output of one goes into the next
  -b, --brute, --brute-force
                        modify Caesar to print every shift up to DIST (default 3)
  -p PERIOD, --period PERIOD
                        specify the period for Bifid to intersperse y- and x-values (deafult 5) 
  -n SEPARATOR, --separator SEPARATOR, --number-separator SEPARATOR
                        the separator to use between numbers in numbered ciphers (default '-')
  -r, --right           use a right shift for Caesar and Vigenere ciphers (default)
  -l, --left            use a left shift for Caesar and Vigenere ciphers
  -e, --encipher        encipher input text (default)
  -d, --decipher        decipher input text

Consult the Wiki for more details: https://gitlab.com/SirDoctorK/cipher/-/wikis/home"


########## Functions


##### Supporting functions


# Return the name of the script
cmd () { echo $(basename $0); }


# Return current time in seconds.milliseconds
now () {
  date +%s.%N
}


# Compute elapsed runtime based on provided start time
runtime () {
  start=$1
  desc="$2"
  end=$(now)
  if $RUNTIME; then echo "     - $desc took $( echo "$end - $start" | bc -l ) seconds"; fi
  #if $RUNTIME; then echo "$end - $start" | bc -l; fi
}


# This just contains a bunch of variable initializations
init_vars () {
  # tr doesn't work with ranges in reverse order ("z-a"), so we have variables containing
  # the whole alphabet in reverse order (and correct order so the code is consistent)
  az="$(echo {a..z} | tr -d ' ')"       # abcd...
  AZ="$(echo {A..Z} | tr -d ' ')"       # ABCD...
  za="$(echo {z..a} | tr -d ' ')"       # zyxw...
  ZA="$(echo {Z..A} | tr -d ' ')"       # ZYXW...

  # Construct Caesar substitution strings so we can just do one 'tr' command for any offset
  azAZ="$az$AZ"
  czArr=( "$azAZ" )
  # Generate all Caesar alphabets
  for i in {1..25}; do
    # Each increasing Caesar alphabet is another rotation of the previous
    czArr[$i]=$(tr "A-Za-z" "B-ZAb-za" <<< ${czArr[$i-1]})
  done

  # Initialize an alphabet array with empty first index for two reasons:
  # 1. This puts 'a' at index 1, so we can directly use A1Z26 numbers as the array index
  # 2. This allows appending alpha[0] to the resulting string ad nauseam with no consequences
  alpha=( '' {a..z} )

  # Control variables
  POLICY="warn"                         # Policy to follow when string doesn't seem to match the cipher
  VERBOSE=0                             # Verbosity level (3 levels)
  RUNTIME=false                         # Print runtime information
  CASE=''                               # Enforce upper/lower-case output
  STRIP=false                           # Remove spaces from input string
  SEQUENTIAL=false                      # Perform cipher operations in sequential order

  # Multiple cipher variables
  NUMBER_SEPARATOR="-"                  # The separator to use between numbers in numbered ciphers

  # Caesar function variables
  CAE=false                             # To perform Caesar (offset)
  DIST=3                                # Distance of shift
  DIRECTION=''                          # Direction of shift (right assumed if not specified)
  BR=false                              # Whether to report progress on each offset up to the distance (brute force mode)

  # Atbash function variables
  ATB=false                             # To perform Atbash (Reverse)

  # A1Z26 function variables
  A1Z=false                             # To perform A1Z26 cipher (Number)

  # Vigenere function variables (also uses DIRECTION variable above)
  VIG=false                             # To perform Vigenere
  VVKEY=''                              # Key for Vigenere and variants
  AK=false                              # To use autokey variant
  OP=''                                 # Operation for Autokey, Keyword, Playfair, Bifid

  # Beaufort function variables
  BEA=false                             # To perform Beaufort
  BKEY=''                               # Key for Beaufort

  # Keyword function variables
  KW=false                              # To perform Keyword
  KKEY=''                               # Key for Keyword

  # Playfair function variables
  PF=false                              # To perform Playfair
  PKEY=''                               # Key for Playfair

  # Bifid function variables
  BIF=false                             # To perform Bifid
  IKEY=''                               # Key for Bifid
  PERIOD=5                              # Period (frequency with which y- and x-values are interspersed)

  # Custom substitution variables
  SUB=false                             # To perform custom substitution
  SUBARGS=( )                           # Array for arguments to the substitution function

  # Random ciphertext variables
  GEN=false                             # To generate the input text
  RAND=false                            # To randomize cipher operations
}


# Initialize starting string from input file, stdin, or random paragraph webpage
init_str () {
  # variable to be used as the resulting string
  rstr=''

  # If set to generate input text, fetch that here
  if $GEN; then
    if [[ $VERBOSE > 0 ]]; then
      echo "Fetching generated input text from '$GENURL'"
    fi
    # Fetch the random paragraph with silent curl and:
    # 1. Grab the first line that matches the correct div id
    # 2. Remove opening and closing div tags (has to be specific because sed is greedy)
    # 3. Remove leading/trailing whitespace
    istr=$(curl -s $GENURL | grep -m 1 "random_word_definition" | sed -e 's/<.*">//g' -e 's/<\/.*>//g' | awk '{$1=$1};NF')
    if [[ $istr == '' ]]; then
      echo "Couldn't get random paragraph as input text" 1>&2
    fi

  # If not set to generate input text, read from normal methods
  else
    if [[ $VERBOSE > 0 ]]; then
      if [ ! $fname ]; then
        echo "No filename, reading from stdin ('${bl}Ctrl-D${nm}' to detach if interactive)"
      else
        echo "Reading from ${bl}$fname${nm}"
      fi
    fi
    # if filename is empty, reads from stdin
    # if cat throws an error, exits script
    if ! istr=$(cat $fname); then exit 1; fi   # original string
  fi

  # Check and exit script if the input string is empty
  if [[ $istr == '' ]]; then
    echo "Empty input string; nothing to do." 1>&2
    exit 1
  fi

  # Any verbosity causes the original input string to be printed
  if [[ $VERBOSE > 0 ]]; then echo -e "  Input string:\n$istr"; fi
}


# Print out brief description of the script and available command line arguments
usage () {
  # An argument being passed inidicates a syntax error in the command
  if [[ $1 != "" ]]; then
    # 'noop' inidcates that no cipher operation was specified
    if [[ $1 == "noop" ]]; then
      echo "No cipher operation specified." 1>&2
      echo "Use at least one of '-[$(echo $opts | tr -d : | grep -oE [A-Z]+)]' to select an operation." 1>&2
    # 'noarg' indicates that there was no argument where expected (or a different getopts error)
    # In either case, the error message is taken care of before calling the usage function
    # If it is neither 'nooop', 'case', nor 'noarg', then $1 contains an invalid option
    elif [[ $1 != "noarg" ]]; then
      echo "$(cmd): invalid option -- '$1'" 1>&2
    fi
    echo "Run '$(cmd) -h' for more information." 1>&2
    exit 1
  fi

  # Default operation of usage is just to report general script and flags description
  echo -e "$HELP_TEXT"
  echo -e "$HELP_FLAGS"
}


# Process key and check for emptiness
get_key () {
  local raw_key="$1"
  # Convert to lowercase
  new_key=${raw_key,,}
  # Remove non-alphanumeric characters
  new_key=${new_key//[^[:alnum:]]/}
  # Check if the string is empty
  if [[ "$new_key" == "" ]]; then
    echo "processed empty cipher key $raw_key (probably no alphanumeric characters)" 1>&2
    usage noarg
  fi
  # Return processed string
  #echo "$new_key"
}


# Function that parses command line arguments
get_args () {
  # process command line arguments and override variables as needed
  # method for flag parsing from here: https://gist.github.com/shakefu/2765260

  # list of single-letter flags supported
  # : = positional argument required
  opts="hi:gvc:sqCO:AZV:G:U:B:K:P:F:I:SRblrdep:n:"

  CIPHERS=()

  for pass in 1 2; do
    while [ -n "$1" ]; do
      case $1 in
        --) 
          shift
          # Grab all remaining arguments into SUBARGS
          while [ -n "$1" ]; do
            SUBARGS+=($1)
            shift
          done
          break
          ;;
        -*) case $1 in
          -h | --help )
            usage
            exit
            ;;
          -i | --input )
            shift
            fname=$1
            ;;
          -g | --generate )
            GEN=true
            ;;
          -v | --verbose )
            (( VERBOSE++ ))
            ;;
          --runtime )
            RUNTIME=true
            ;;
          -c | --case )
            shift
            # This will assign the CASE variable and check the return status
            if ! CASE=$(canoncase $1); then
              usage "case"
            fi
            ;;
          -s | --strip | --strip-spaces )
            STRIP=true
            ;;
          -m | --policy | --mismatch-policy )
            shift
            # This will assign the CASE variable and check the return status
            if ! POLICY=$(canon_policy $1); then
              usage "policy"
            fi
            ;;
          -C | --caesar )
            CIPHERS+=( "cae,3" )
            ;;
          -O | --offset )
            shift
            if [[ $1 =~ ^[0-9]+$ ]]; then 
              CIPHERS+=( "cae,$1" )
            else 
              echo "Need to specify offset distance." 1>&2
              usage noarg
            fi
            ;;
          -A | --atbash )
            CIPHERS+=( "atb,-" )
            ;;
          -Z | --a1z26 )
            CIPHERS+=( "a1z,-" )
            ;;
          -V | -G | --vigenere | --gronsfeld  )
            shift
            get_key "$1"
            CIPHERS+=( "vig,$new_key" )
            ;;
          -U | --autokey  )
            shift
            get_key "$1"
            CIPHERS+=( "ak,$new_key" )
            # Set decipher operation if not already set
            # Allows '-d' and '-e' to take effect if placed before this flag
            if [[ $OP == '' ]]; then OP='en'; fi
            ;;
          -B | --beaufort )
            shift
            get_key "$1"
            CIPHERS+=( "bea,$new_key" )
            ;;
          -K | --keyword )
            shift
            get_key "$1"
            CIPHERS+=( "kw,$new_key" )
            ;;
          -P | --polybius )
            shift
            get_key "$1"
            CIPHERS+=( "ply,$new_key" )
            ;;
          -F | --playfair )
            shift
            get_key "$1"
            CIPHERS+=( "pf,$new_key" )
            ;;
          -I | --bifid )
            shift
            get_key "$1"
            CIPHERS+=( "bif,$new_key" )
            ;;
          -S | --substitute | --substitution )
            CIPHERS+=( "sub,-" )
            ;;
          -R | --random )
            CIPHERS+=( "rand,-" )
            ;;
          -b | --brute | --brute-force )
            BR=true
            ;;
          -q | --seq | --sequential )
            SEQUENTIAL=true
            ;;
          -r | --right )
            DIRECTION=R
            ;;
          -l | --left )
            DIRECTION=L
            ;;
          -e | --encipher )
            OP='en'
            # Set direction if not already set
            # Allows '-l' and '-r' to take effect if placed before this flag
            if [[ $DIRECTION == '' ]]; then DIRECTION=R; fi
            ;;
          -d | --decipher )
            OP='de'
            if [[ $DIRECTION == '' ]]; then DIRECTION=L; fi
            ;;
          -p | --period )
            shift
            if [[ $1 =~ ^[0-9]+$ ]]; then
              PERIOD=$1
            else
              echo "Invalid period $1; will run with default of $PERIOD." 1>&2
            fi
            ;;
          -n | --separator | --number-separator )
            shift
            NUMBER_SEPARATOR="$1"
            ;;
          --*)
            usage $1
            exit
            ;;
          -*)
            if [ $pass -eq 1 ]; then ARGS="$ARGS $1";
            else usage $1; fi;;
          esac;;
        *)  
          if [ $pass -eq 1 ]; then ARGS="$ARGS $1";
          else usage $1; fi;;
      esac
      shift
    done
    if [ $pass -eq 1 ]; then 
      ARGS=$(getopt $opts $ARGS)
      if [ $? != 0 ]; then usage noarg; fi
      set -- $ARGS
    fi
  done

  CIPHERS=( $(process_cipher_list "${CIPHERS[@]}") )

  # Check for no cipper operations
  if [[ ${#CIPHERS[@]} < 1 ]]; then
    usage noop
  fi

  # Set default direction if unset
  if [[ $DIRECTION == '' ]]; then DIRECTION=R; fi

  # If capital letters are requested, we will translate lowercase to uppercase
  if [[ $CASE == C ]]; then SET1='[:lower:]'; SET2='[:upper:]'
  # If small letters are requested, we will translate uppercase to lowercase
  elif [[ $CASE == S ]]; then SET1='[:upper:]'; SET2='[:lower:]'
  # If no case is requested, we will translate uppercase to uppercase
  else SET1='[:upper:]'; SET2='[:upper:]'
  fi

  if [[ $VERBOSE > 1 ]]; then echo "Cipher operations: ${CIPHERS[@]}"; fi
}


# Look for random ciphers in list and pair them with randomization hints
process_cipher_list () {
  local ciphers=("$@")
  local random_ciphers=()
  local target="rand"
  local cipher=""
  local previous_cipher="none"
  compare_ciphers () {
    local cipher1="$1"
    local cipher2="$2"
    if [[ "$cipher1" == "$target" ]]; then
      # Match [Random, Random] (consume first, leave second until we know the next item)
      if [[ "$cipher2" == "$target" ]]; then
        random_ciphers+=( "$target,-" )
        previous_cipher="$cipher2"
      # Match [Random, Cipher] (consume both)
      else
        random_ciphers+=( "$target,$cipher2" )
        previous_cipher="none"
      fi
    elif [[ "$cipher1" == "none" ]]; then
      # Match [None, Any] (skip the None)
      previous_cipher="$cipher2"
    else
      # Match [Cipher, Random] (consume both)
      if [[ "$cipher2" == "$target" ]]; then
        random_ciphers+=( "$target,$cipher1" )
        previous_cipher="none"
      # Match [Cipher, Cipher] (skip first)
      else
        previous_cipher="$cipher2"
      fi
    fi
  }
  for item in "${ciphers[@]}"; do
    cipher=$(echo "$item" | cut -d ',' -f 1)
    # Compare and update previous cipher
    compare_ciphers "$previous_cipher" "$cipher"
  done
  # One more compare to handle trailing Random operations
  compare_ciphers "$previous_cipher" "none"
  # Return the appropriate array
  if [[ ${#random_ciphers[@]} > 0 ]]; then
    echo "${random_ciphers[@]}"
  else
    echo "${ciphers[@]}"
  fi
}


# Identify whether string contains primarily digits or alpha
contains () {
  # Identify the contents by the first or second character
  if [[ "$istr" =~ ^.?[0-9] ]]; then echo "digits"
  elif [[ "$istr" =~ ^.?[A-Za-z] ]]; then echo "letters"
  else echo "unknown"
  fi
}


# Canonicalize case designator
canoncase () {
  # Using regex match to see if the provided case (changed to lowercase)
  # matches the start of a valid designator
  if [[ "capital" =~ ^${1,,} ]] || [[ "uppercase" =~ ^${1,,} ]]; then
    echo C
  elif [[ "small" =~ ^${1,,} ]] || [[ "lowercase" =~ ^${1,,} ]]; then
    echo S
  elif [[ "none" =~ ^${1,,} ]] || [[ "original" =~ ^${1,,} ]]; then
    echo N
  else
    echo "Invalid case designator '$1'" 1>&2
    echo "Specify all or part of 'capital', 'uppercase', 'small', or 'lowercase'" 1>&2
    return 1
  fi
}


# Canonicalize policy
canon_policy () {
  # Using regex match to see if the provided case (changed to lowercase)
  # matches the start of a valid designator
  if [[ "warn" =~ ^${1,,} ]]; then
    echo warn
  elif [[ "continue" =~ ^${1,,} ]]; then
    echo continue
  elif [[ "skip" =~ ^${1,,} ]]; then
    echo skip
  else
    echo "Invalid input policy designator '$1'" 1>&2
    echo "Specify all or part of 'warn', 'continue', or 'skip'" 1>&2
    return 1
  fi
}


# Print warning if string doesn't seem to work with desired cipher
confirm () {
  # If an assumption has been specified, return from the function with the appropriate return value
  if [ $POLICY == "continue" ]; then return; 
  elif [ $POLICY == "skip" ]; then return 1;
  else
    echo -e "String starts with $CONT, which the specified cipher doesn't expect." 1>&2
    echo -e "The result is unlikely to be useful." 1>&2
    echo -e "(Specify a mismatch policy ('-m') to suppress this message.)" 1>&2
    return
  fi
}


# Return the index of the provided character
get_index () {
  # If the provided character is a digit, return it (for Vigenere Gronsfeld variant)
  if [[ $1 =~ ^[0-9]$ ]]; then
    # We need to return $1 plus one since the vigenere() function decrements this result
    # and when using Gronsfeld, we need 0 and 1 to work directly
    echo $(($1 + 1))
    return
  fi
  
  # Loop through all characters in the alphabet
  # Using $j since this function is often called within another loop using $i
  for j in ${!alpha[@]}; do 
    # The ${1,,} construct gives the lower-case version of the first function parameter
    if [ "${alpha[$j]}" == "${1,,}" ]; then 
      # Typically this echo statement is caught by a variable and not printed to stdout
      echo $j
      return
    fi
  done
}


# Initialize alphabet with specific options for Keyword, Playfair, and Bifid
alphinit () {
  local init_method=$1
  local tempkey=${2,,}
  # Reinitialize alphabet array to standard sequence
  alpha=( '' {a..z} )
  # Array index of the start of the untouched alphabet
  # (Alphabet Start Index)
  # All functions that use this pop off the 0th element
  local asi=0

  # Argument to the function tells it which rules to use to construct the alphabet
  # It will grab the lower case version of that key
  case $init_method in
    b | k ) # Beaufort and keyword should both have a regular alphabet
      alpha=( {a..z} )
      ;;
    p | f | i ) # Polybius ciphers need 'j' as 'i'
      tempkey="$(tr j i <<< ${tempkey,,})"
      alpha=( {a..i} {k..z} )
      ;;
    * )
      return
      ;;
  esac

  # Construct the substitution alphabet
  # Loop through characters in the key (by default the key is empty)
  for (( i=0; i<${#tempkey}; i++ )); do
    # Get one character from the key at position $i
    k=${tempkey:$i:1}
    # Verify that $k is a letter
    if [[ $k =~ ^[a-z]$ ]]; then
      # Get the index of the key character
      kin=$(get_index $k)
      # If the key character index is at or past the alphabetic start index,
      # we are at its first occurrence in the key
      if (( "$kin" >= "$asi" )); then
        # Loop from $kin down to $asi (if equal, will do nothing)
        for (( j=$kin; j>$asi; j-- )); do
          # Move the characters right one space in this range
          alpha[$j]=${alpha[$j-1]}
        done
        # Set the alphabet at $asi to the current key character
        alpha[$asi]=$k
        # Increment $asi
        (( asi++ ))
      fi
      # If this is not the first occurrence of the key character, nothing to do
    fi
    # If $k is not a letter, we don't need to do anything with it
  done
}


# Print current alphabet in Polybius square form for related ciphers (for debugging and tutorial)
print_square () {
  echo "  1 2 3 4 5"
  echo "1 ${alpha[0]} ${alpha[1]} ${alpha[2]} ${alpha[3]} ${alpha[4]}"
  echo "2 ${alpha[5]} ${alpha[6]} ${alpha[7]} ${alpha[8]} ${alpha[9]}"
  echo "3 ${alpha[10]} ${alpha[11]} ${alpha[12]} ${alpha[13]} ${alpha[14]}"
  echo "4 ${alpha[15]} ${alpha[16]} ${alpha[17]} ${alpha[18]} ${alpha[19]}"
  echo "5 ${alpha[20]} ${alpha[21]} ${alpha[22]} ${alpha[23]} ${alpha[24]}"
}


# Get element from array with 2d indices
get2d () {
  # Ensure the parameters are negative or positive digits before proceeding
  if ! [[ $1 =~ ^-?[0-9]$ ]] || ! [[ $2 =~ ^-?[0-9]$ ]]; then
    echo "get2d($1 $2): Invalid parameters" 1>&2
    return
  fi

  # Perform modulo to account for positive rollover
  ix=$(( $1 % 5 ))
  iy=$(( $2 % 5 ))

  # Account for negative X-value rollover
  if (( "$1" < "0" )); then
    # The X or Y value should be 5 "plus" the negative number
    # E.g., 5 + -1 = 5 - 1 = 4 (last column)
    ix=$(( 5 + $1 ))
  fi

  # Same for negative Y-value rollover
  if (( "$2" < "0" )); then
    iy=$(( 5 + $2 ))
  fi
  
  # Observe the layout in the print_square() function if this doesn't make sense
  index=$(( $iy * 5 + $ix ))

  # The "return value" captured when the function is called
  echo ${alpha[$index]}
}


# Get element from array with 2d indexes
get_square_item () {
  # Ensure the parameters are negative or positive digits before proceeding
  if ! [[ $1 =~ ^-?[0-9]$ ]] || ! [[ $2 =~ ^-?[0-9]$ ]]; then
    echo "get_square_item($1 $2): Invalid parameters" 1>&2
    return
  fi
  # Perform modulo to account for positive rollover 
  # (and -1 to get back to normal 0-based indexing)
  local row=$(( ($1-1) % 5 ))
  local column=$(( ($2-1) % 5 ))


  # Account for negative column rollover
  if (( "$column" < "0" )); then
    # The X or Y value should be 5 "plus" the negative number
    # E.g., 5 + -1 = 5 - 1 = 4 (last column)
    column=$(( 5 + $column ))
  fi

  # Same for negative row rollover
  if (( "$row" < "0" )); then
    row=$(( 5 + $row ))
  fi
  
  # Observe the layout in the print_square() function if this doesn't make sense
  local index=$(( $row * 5 + $column ))
  # if [[ $VERBOSE > 2 ]]; then
  #   echo "'get_square_item($1 $2)' yielded regular index $index" > /dev/stderr
  # fi

  # The "return value" captured when the function is called
  echo ${alpha[$index]}
}


# Get 2d indexes for specified item
get_square_indexes () {
  # Get regular index
  reg_index=$(get_index $1)
  if [[ -z "$reg_index" ]]; then
    return 1
  else
    # Get current and next row/column position, adding one to make it one-based
    # Observe the layout of print_square() if this doesn't make sense
    local row=$(( ($reg_index / 5) + 1 ))
    local column=$(( ($reg_index % 5) + 1 ))
    # Return as space-separated values
    echo "$row $column"
  fi
}


# Wait for Enter to continue, used in tutorials
wait_enter() {
  # If no argument provided, assume solving the cipher
  if [[ -z "$1" ]]; then
    ACTION="Solve the cipher"
  # Argument to function, if provided, is the action that we should suggest the user to do
  else
    ACTION="$1"
  fi

  # Try to wait for Enter to continue, else just print a newline and continue
  if read -p "$ACTION on your own if you wish, then press ${bl}Enter${nm} to continue: " -r; then
    true
  else # If `read` fails, it's non-interactive and we'll assume no
    echo
  fi
  # Extra newline to get the spacing right
  echo
}


##### Core functions


# Caesar cipher (default to 3-char offset)
caesar () {
  # If a parameter is provided to the function, it will offset the provided string (for Vigenere and tutor function)
  if [ "$1" != "" ]; then rstr=$1
  # Otherwise it will offset $istr
  else rstr="$istr"
  fi

  #cae_start=$(now)

  if $BR; then
    # If using brute force mode, the max distance should be enforced by setting anything higher to 25
    if (( $DIST > 25 )); then DIST=25; fi
    # This is the original logic, which is still necessary for the brute force functionality
    if [[ $DIRECTION == "L" ]]; then
      # Using $j since this function is sometimes called within another loop using $i
      for (( j=1; j<=$DIST; j++ )); do
        # Shift characters left
        rstr="$(tr "A-Za-z" "ZA-Yza-y" <<< "$rstr")"
        # If we're doing a brute force run, print current offset
        if $BR; then 
          report "  Processed with ${bl}Caesar $DIRECTION $j${nm}:"
        fi
      done
    else # If no direction specified, assume right (encipher)
      for (( j=1; j<=$DIST; j++ )); do
        # Shift characters right
        rstr=$(tr "A-Za-z" "B-ZAb-za" <<< "$rstr")
        # If we're doing a brute force run, print current offset
        if $BR; then 
          report "  Processed with ${bl}Caesar $DIRECTION $j${nm}:"
        fi
      done
    fi
  else
    # This is the new logic, which requires only 1 'tr' command per function call.
    # This dramatically improves performance, expecially when the Vigenere calls this function.

    # Offset 26 is equivalent to 0
    # To make this new Caesar approach work, it can't be higher than 25
    DIST=$(( $DIST % 26 ))

    #tr_start=$(now)

    # Depending on direction, translate from or to the correct Caesar alphabet
    if [[ $DIRECTION == "L" ]]; then
      rstr=$(tr "${czArr[$DIST]}" "$azAZ" <<< "$rstr")
    else
      rstr=$(tr "$azAZ" "${czArr[$DIST]}" <<< "$rstr")
    fi

    #runtime $tr_start "running tr command"
  fi
  #runtime "$cae_start" "processing Caesar with offset $DIST"
}


# Caesar tutorial
caesar_tutor () {
  echo
  echo "== Caesar Cipher tutorial =="
  echo
  echo "The Ceasar Cipher shifts all letters in the string by DIST positions in the alphabet."
  echo "The default Caesar distance is 3."
  echo "Typically a RIGHT shift is used to encipher text and LEFT to decipher."
  echo
  echo "To perform the Caesar cipher with the current settings,"
  echo " 1. Locate each character in the input string"
  echo " 2. Replace it with the character that is $DIST spaces to the $DIRECTION:"
  echo
  # Right justify 10 spaces
  printf "%10s" "input"
  echo " = $istr"
  printf "%10s" "operation"
  echo " = $DIRECTION $DIST"
  echo

  # Print out normal alphabet 
  echo {a..z}
  echo

  wait_enter

  # If verbosity is over 2, we will step by step explain each word in the string
  if [[ $VERBOSE > 2 ]]; then
    # If an instr has been provided, set it to istr
    if [ "$1" != "" ]; then instr=$1
    # Otherwise set it to $istr
    else instr="$istr"
    fi

    # Using c-style loop to iterate over individual characters in $istr
    for (( i=0; i<${#istr}; i++ )); do 
      # Get one character from $istr at position $i
      c=${istr:$i:1}
      case $c in
        [a-z] | [A-Z] )
          caesar $c
          echo "  '$c' -> '$rstr'"
          ;;
        [$'\n'] )
          echo -e "  newline\n"
          ;;
        * )
          echo "  '$c' -> '$c'"
          ;;
      esac
    done
    echo
  fi
}


# Atbash cipher (reversed letters)
atbash () {  
  # If a parameter is provided to the function, it will process the provided string (for tutor function)
  if [ "$1" != "" ]; then rstr="$1"
  # Otherwise it will offset $istr
  else rstr="$istr"
  fi

  # first reverse lowercase letters
  rstr="$(tr $az $za <<< "$rstr")"
  # then reverse uppercase letters
  rstr="$(tr $AZ $ZA <<< "$rstr")"
}


# Atbash tutorial
atbash_tutor () {
  echo
  echo "== Atbash Cipher tutorial =="
  echo
  echo "The Atbash Cipher translates all letters in the string to their complement in the alphabet."
  echo "It is a reciprocal cipher; the same steps are used to encipher and decipher text."
  echo
  echo "To perform the Atbash cipher,"
  echo " 1. Locate each input character in the normal alphabet"
  echo " 2. Replace it with the character in the same position in the reversed alphabet:"
  echo
  # Right justify 10 spaces
  printf "%10s" "input"
  echo " = $istr"
  echo

  # Print forward and backward sequence of alphabetic characters
  echo {a..z}
  echo {z..a}
  echo

  wait_enter

  # If verbosity is over 2, we will step by step explain each word in the string
  if [[ $VERBOSE > 2 ]]; then
    # If an instr has been provided, set it to istr
    if [ "$1" != "" ]; then instr=$1
    # Otherwise set it to $istr
    else instr="$istr"
    fi

    # Using c-style loop to iterate over individual characters in $istr
    for (( i=0; i<${#istr}; i++ )); do 
      # Get one character from $istr at position $i
      c=${istr:$i:1}
      case $c in
        [a-z] | [A-Z] )
          atbash $c
          echo "  '$c' -> '$rstr'"
          ;;
        [$'\n'] )
          echo -e "  newline\n"
          ;;
        * )
          echo "  '$c' -> '$c'"
          ;;
      esac
    done
    echo 
  fi
}


# A1Z26 cipher (numbered position in alphabet)
a1z26 () {
  # Index of next letter
  idx=''
  # Resulting string
  rstr=''

  # Show tutorial explanation if verbosity 2+
  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == A1Z26 Cipher tutorial =="
    echo
    echo "The A1Z26 Cipher represents letters by their numberic position in the alphabet."
    echo "This is summarized in the name; A=1, Z=26."
    echo
    echo "To perform the A1Z26 cipher,"
    echo " 1. Locate each letter or number in the input string"
    echo " 2. Replace it with the correlating item of the other type"
    echo "    a. If you are enciphering into numbers, add the separator ('$NUMBER_SEPARATOR') between successive numbers"
    echo "    b. If you are deciphering from numbers, remove the separators between the numbers"
    echo "    c. Pass through spaces, punctuation, other special characters"
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    echo

    # Print out numbered indices for the alphabet
    for n in {1..26}; do
      # %- = left justify; 3 = width; s = format string of characters
      printf "%-3s" $n
    done
    echo

    # Print out a to z to align with the numbers
    for c in {a..z}; do
      printf "%-3s" $c
    done
    echo
    echo

    wait_enter
  fi

  # Using c-style loop to iterate over individual characters in $istr
  for (( i=0; i<${#istr}; i++ )); do 
    # Get one character from $istr at position $i
    c=${istr:$i:1}
    # Original and new content added this round
    oc=''
    nc=''
    case $c in
      [a-z] | [A-Z] )
        oc+="$c"
        # If the current last character in the resulting string is a digit,
        # we will need a '-' to distinguish the different indices 
        if [[ $rstr =~ [0-9]$ ]]; then nc+="$NUMBER_SEPARATOR"; fi
        # Append the index of the input char
        nc+=$(get_index $c)
        ;;
      [0-9] )
        # Add the digit character to the index variable
        idx+=$c
        ;;
      * )
        if [[ "$idx" != 0 ]]; then
          oc+="$idx"
        fi

        # A dash (or any other character) indicates the end of a numbered index.
        # Add to the resulting string the alphabet character at the current index.
        # This sometimes adds the first item in the array to the string,
        # which doesn't matter since the first item is ''
        # The old solution was: if [ $idx ]; then rstr+=${alpha[$idx-1]}; fi
        nc+=${alpha[$idx]}
        # Clear the index variable for next time
        idx=''
        # If the special character is anything other than a newline,
        # we should report it as part of the original content
        if [[ "$c" != $'\n' ]]; then
          #echo "non-newline special char"
          oc+="$c"
          # If the special character is also not a dash, should be passed on to result
          if [[ "$c" != "$NUMBER_SEPARATOR" ]]; then
            nc+="$c"
          fi
        fi
        ;;
    esac

    # If step-by-step (verb 3+), output original content and new content
    if [[ $VERBOSE > 2 ]] && [[ $nc != '' ]]; then
      # add ' to start of original content for easier readability
      oc="'$oc"
      # Right justify (pad to the left) with two spaces beforehand
      printf "  %4s" "$oc"
      echo "' -> '$nc'"
    fi

    # Append new content to resulting string
    rstr+="$nc"

    # Account for newline characters separately for better step-by-step output
    if [[ "$c" == $'\n' ]]; then
      rstr+="$c"
      if [[ $VERBOSE > 2 ]]; then
        echo -e "    newline\n"
      fi
    fi
  done

  if [ $idx ]; then
    # One more append if there is a remaining index variable
    rstr+=${alpha[$idx]}
    # Report extra append if step-by-step
    if [[ $VERBOSE > 2 ]]; then
      oc+="'$idx"
      nc+=${alpha[$idx]}
      printf "  %4s" "$oc"
      echo "' -> '$nc'"
      # Extra newline
      echo
    fi
  fi
}


# Vigenere cipher (offset cipher based on per-letter alphabetic index in provided key)
vigenere () {
  # working string
  wstr=''
  # resulting string
  rstr=''
  # index for key
  kin=0


  # If enciphering with the autokey method, the input string should be appended to the key
  if $AK && [[ $OP == 'en' ]]; then VKEY+="$istr"; fi
  # Remove non-alpha chars from key (may be added from $istr)
  VKEY="$(echo $VKEY | tr -cd '[:alnum:]')"
  
  # Show tutorial explanation if verbosity 2+
  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == Vigenere Cipher tutorial =="
    echo
    echo "The Vigenere Cipher is a more elaborate Caesar cipher in which the offset varies per letter."
    echo "The key provided to the cipher determines this per-letter offset."
    if $AK; then
      echo
      echo "The Autokey variant has been selected, so the plain text should be appended to the key."
      if [[ $OP == 'en' ]]; then
        echo "It is currently set to *encipher*, so the input text can be immediately appended to the key."
      else
        echo "It is currently set to *decipher*, so the plain text must be appended to the key as it is deciphered."
      fi
    fi
    echo
    echo "To perform the Vigenere cipher,"
    echo " 1. Line each alphabetic input character with the corresponding alpha or numeric character in the key"
    echo "    a. Pass through all non-alpha input characters"
    echo "    b. Repeat the key as needed"
    echo " 2. Locate the key character's index in the alphabet (or literal value if a digit)"
    echo " 3. Replace the input character with the character that many characters to the $DIRECTION"
    if $AK && [[ $OP == 'de' ]]; then
      echo "    a. (Autokey) Add the new character to the end of the key"
    fi
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    printf "%10s" "key"
    echo " = $VKEY"
    printf "%10s" "operation"
    echo " = ${OP}cipher $DIRECTION"
    echo

    # Print out numbered indices for the alphabet (like A1Z26)
    for n in {1..26}; do
      printf "%-3s" $n
    done
    echo

    # Print out a to z to align with the numbers
    for c in {a..z}; do
      printf "%-3s" $c
    done
    echo; echo

    wait_enter
  fi

  #process_start=$(now)

  # using c-style loop to iterate over individual characters in $istr
  for (( i=0; i<${#istr}; i++ )); do 
    # length of the key
    # this is recalculated each loop for compatibility with deciphering autokey
    klen=${#VKEY}
    # Get one character from $istr at position $i
    c=${istr:$i:1}
    # Check if the character is a letter
    if [[ $c =~ ^[[:alpha:]]$ ]]; then
      # Find the corresponding offset character using modulo so it wraps around
      # when $istr is longer than $VKEY, which is a common occurrence (unless autokey)
      k=${VKEY:$(($kin % $klen)):1}
      # Assign the $DIST varaible for the offset function to the index of the key char
      # minus one since A should match with 0 offset
      DIST=$(( $(get_index $k) - 1 ))
      # Perform offset only on the current char from $istr
      caesar $c
      # Internally, the Caesar function uses $rstr, so this function uses $wstr until the end
      wstr+=$rstr
      # If deciphering with the autokey method, append deciphered alphanumeric characters to the key
      if $AK && [[ $OP == 'de' ]]; then VKEY+=$rstr; fi
      # Increment key index
      (( kin++ ))

      # Step-by-step commentary
      if [[ $VERBOSE > 2 ]]; then
        echo "  '$c' -> '$rstr' ( $k = $DIST )"
      fi
    else
      # Key characters should only match up with alpha characters, 
      # so kin is only incremented when encountering alpha in the input (previous branch)
      # Here, we just append whatever other char we have to wstr
      wstr+=$c

      if [[ $VERBOSE > 2 ]]; then
        if [[ "$c" == $'\n' ]]; then
          echo -e "  newline\n"
        else
          echo "  '$c' -> '$c'"
        fi
      fi
    fi
  done

  #runtime "$process_start" "processing Vigenere cipher"

  if [[ $VERBOSE > 2 ]]; then echo; fi

  # Store the working string into the resulting string for later printing
  rstr="$wstr"
}


# Beaufort cipher (symmetric substitution cipher similar to the Vigenere cipher)
beaufort () {
  # Resulting string
  rstr=''

  # index for key
  kin=0
  # length of the key
  klen=${#BKEY}

  # Remove spaces from key
  BKEY="$(echo $BKEY | tr -d ' ')"

  # Show tutorial explanation if verbosity 2+
  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == Beaufort Cipher tutorial =="
    echo
    echo "The Beaufort cipher is similar to the Vigenere cipher in that it processes"
    echo "each input character differently based on the provided key."
    echo "It is different in that it is a *reciprocal* cipher,"
    echo "so the same procedure is used to encipher and decipher text."
    echo
    echo "To perform the Beaufort cipher:"
    echo " 1. Line up each alphabetic input character with the corresponding alpha or numeric character in the key"
    echo "    a. Pass through all non-alpha input characters"
    echo "    b. Repeat the key as needed"
    echo " 2. Find the alphabetic indexes for both the input and key characters"
    echo " 3. The resulting character is the character *input* spaces to the L from the *key* character"
    echo "    a. To summarize, result = key - input"
    echo "    b. Refer to the positive or negative indices listed below depending on the result"
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    printf "%10s" "key"
    echo " = $BKEY"
    printf "%10s" "formula"
    echo " = result = key - input"
    echo

    # Print out positive indices for the alphabet starting at 0
    for n in {0..25}; do
      printf "%-3s" $n
    done
    echo " - positive indices"

    # Print out a to z to align with the numbers
    for c in {a..z}; do
      printf "%-3s" $c
    done
    echo

    # Print out negative indices for the alphabet
    for n in {26..1}; do
      printf "%-3s" $n
    done
    echo " - negative indices"
    echo

    wait_enter
  fi

  # using c-style loop to iterate over individual characters in $istr
  for (( i=0; i<${#istr}; i++ )); do 
    # Get one character from $istr at position $i
    c=${istr:$i:1}
    # If the character is a letter
    if [[ $c =~ ^[[:alpha:]]$ ]]; then
      # Find the corresponding key character using modulo so it wraps around
      # when $istr is longer than $BKEY, which is a common occurrence
      k=${BKEY:$(($kin % $klen)):1}
      # Find alphabetic indices for key and input character
      aik=$(get_index $k)
      aic=$(get_index $c)
      # The index of the result character is 
      # the index of the key character minus the index of the input character
      idx=$(( $aik - $aic ))
      # Add the result character to the result string
      nc=${alpha[$idx]}
      rstr+=$nc
      # Increment key index
      (( kin++ ))

      # Step-by-step commentary
      if [[ $VERBOSE > 2 ]]; then
        echo "  '$c' -> '$nc' -- ( $k=$aik ) - ( $c=$aic ) = ( $idx=$nc )"
      fi
    else
      # Key characters should only match up with alpha characters, 
      # so kin is only incremented when encountering alpha in the input (previous branch)
      # Here, we just append whatever other char we have to wstr
      rstr+="$c"

      if [[ $VERBOSE > 2 ]]; then
        if [[ "$c" == $'\n' ]]; then
          echo -e "  newline\n"
        else
          echo "  '$c' -> '$c'"
        fi
      fi
    fi
  done
  
  if [[ $VERBOSE > 2 ]]; then echo; fi
}


# Keyword cipher (monoalphabetic substitution cipher determined by a keyword)
keyword () {
  # If a parameter is provided to the function, it will process the provided string (for tutor function)
  if [ "$1" != "" ]; then rstr="$1"
  # Otherwise it will offset $istr
  else rstr="$istr"
  fi

  # Construct upper and lowercase cipher alphabet strings (Key-to-Z)
  kz="$(echo ${alpha[@]} | tr -d ' ')"
  KZ="${kz^^}"

  # The actual operation depends on whether we're enciphering or deciphering
  if [[ $OP == 'de' ]]; then
    # To decipher, translate k-z and K-Z back to a-z and A-Z
    rstr="$(tr $kz $az <<< "$rstr")"
    rstr="$(tr $KZ $AZ <<< "$rstr")"
  else
    # To encipher (default), translate a-z and A-Z to k-z and K-Z
    rstr="$(tr $az $kz <<< "$rstr")"
    rstr="$(tr $AZ $KZ <<< "$rstr")"
  fi
}


# Mixed alphabet tutorial, used by Keyword, Playfair, and Bifid
malpha_tutor () {
  KEY="$1"
  echo "To construct the mixed alphabet:"
  echo " 1. Write out all ${bl}unique${nm} letters in the provided key: ${bl}$KEY${nm}"
  echo "    a. Only the ${bl}first occurrence${nm} of duplicate letters should remain"
  echo " 2. After the key, write out all the remaining letters of the alphabet in their regular order"
  echo
  echo "You may prefer to think of it in the way that the script accomplishes it:"
  echo " 1. Start with the normal alphabet"
  echo " 2. For each ${bl}unique${nm} letter in the provided key:"
  echo "    a. Move it to just before the first letter in the ${bl}untouched${nm} alphabet"
  echo
  printf "%10s" "key"
  echo " = $KEY"
  echo

  wait_enter "Construct the alphabet"
}


# Keyword tutorial
keyword_tutor () {
  echo
  echo " == Keyword Cipher tutorial =="
  echo
  echo "The Keyword cipher is a monoalphabetic substitution cipher that"
  echo "uses the provided key to construct a mixed cipher alphabet."
  echo
  malpha_tutor "$KKEY"
  echo "Now that we have the mixed alphabet constructed:"
  if [[ $OP == 'de' ]]; then
    echo " 1. Locate each input character in the mixed alphabet"
    echo " 2. Replace it with the character in the same position in the normal alphabet"
  else
    echo " 1. Locate each input character in the normal alphabet"
    echo " 2. Replace it with the character in the sameposition in the mixed alphabet"
  fi
  echo
  # Right justify 10 spaces
  printf "%10s" "input"
  echo " = $istr"
  echo

  # Print normal and mixed sequence of alphabetic characters
  echo {a..z}

  for c in ${alpha[@]}; do 
    echo -n "$c "
  done
  echo; echo

  wait_enter

  # If verbosity is over 2, we will step by step explain each word in the string
  if [[ $VERBOSE > 2 ]]; then
    # If an instr has been provided, set it to istr
    if [ "$1" != "" ]; then instr=$1
    # Otherwise set it to $istr
    else instr="$istr"
    fi

    # Using c-style loop to iterate over individual characters in $istr
    for (( i=0; i<${#istr}; i++ )); do 
      # Get one character from $istr at position $i
      c=${istr:$i:1}
      case $c in
        [a-z] | [A-Z] )
          keyword $c
          echo "  '$c' -> '$rstr'"
          ;;
        [$'\n'] )
          echo -e "  newline\n"
          ;;
        * )
          echo "  '$c' -> '$c'"
          ;;
      esac
    done
    echo 
  fi
}


# Polybius square tutorial, used by Playfair and Bifid
psquare_tutor () {
  echo "To construct the Polybius square in a 5x5 grid:"
  echo " 1. Remove the letter '${bl}j${nm}' from the alphabet"
  echo "    a. We only have 25 total spaces in the square"
  echo "    b. Instances of '${bl}j${nm}' will be replaced with '${bl}i${nm}'"
  echo " 2. Place the ${bl}first 5${nm} characters in the alphabet on the first row"
  echo " 3. Place the ${bl}next 5${nm} characters on the second row and ${bl}repeat${nm} for the other 3 rows"
  echo
  # Print mixed alphabet with spaces between letters
  for c in ${alpha[@]}; do 
    echo -n "$c "
  done
  echo; echo

  wait_enter "Construct the square"
}


# Polybius cipher (numbered substitution cipher using coordinates in Polybius square)
polybius () {
  # Playfair considers 'j' as 'i'
  # Store substituted string in $wstr
  wstr="$(tr j i <<< ${istr})"
  # Index of next letter
  idx=''
  # Resulting string
  rstr=''

  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == Polybius Cipher tutorial =="
    echo
    echo "The Polybius cipher is the most basic cipher that uses the Polybius square."
    echo "Like the A1Z26 cipher, it encodes letters as numbers representing their position."
    echo "Unlike A1Z26, which uses a regular alphabetic order, Polybius uses two digits for each letter,"
    echo "representing the row and column position on a polybius square."
    echo
    malpha_tutor "$PKEY"
    psquare_tutor
    echo "Now that we have the Polybius square, we can perform the cipher:"
    echo "  1. Replace all j's in the input string with i"
    echo "  2. Locate each input character on the Polybius square"
    echo "    a. Enciphering: replace each letter with the row number, then column number"
    echo "      i. Add the separator character ('$NUMBER_SEPARATOR') between adjacent number pairs"
    echo "    b. Deciphering: interpret each number pair as row number, then column number"
    echo "    b. Pass through spaces, punctuation, other special characters"
    echo
    print_square
    echo
    wait_enter
  fi

  # Using c-style loop to iterate over individual characters in $istr
  for (( i=0; i<${#istr}; i++ )); do 
    # Get one character from $istr at position $i
    c=${istr:$i:1}
    # Original and new content added this round
    oc=''
    nc=''
    case $c in
      [a-z] | [A-Z] )
        oc+="$c"
        # If the current last character in the resulting string is a digit,
        # we will need the separator to distinguish the different indices 
        if [[ $rstr =~ [0-9]$ ]]; then nc+="$NUMBER_SEPARATOR"; fi
        # Append the square indexes of the input char
        index_pair=$(get_square_indexes $c)
        # Remove spaces
        nc+="${index_pair// /}"
        ;;
      [0-9] )
        # Add the digit character to the index variable
        idx+=$c
        ;;
      * )
        if [[ "$idx" != 0 ]]; then
          oc+="$idx"
        fi

        # A dash (or any other character) indicates the end of a numbered index.
        # Add to the resulting string the alphabet character at the current index.
        # This sometimes adds the first item in the array to the string,
        # which doesn't matter since the first item is ''
        # The old solution was: if [ $idx ]; then rstr+=${alpha[$idx-1]}; fi
        if [ $idx ]; then
          local row=${idx:0:1}
          local column=${idx:1:1}
          nc+=$(get_square_item $row $column)
        fi
        # Clear the index variable for next time
        idx=''
        # If the special character is anything other than a newline,
        # we should report it as part of the original content
        if [[ "$c" != $'\n' ]]; then
          #echo "non-newline special char"
          oc+="$c"
          # If the special character is also not a dash, should be passed on to result
          if [[ "$c" != "$NUMBER_SEPARATOR" ]]; then
            nc+="$c"
          fi
        fi
        ;;
    esac

    # If step-by-step (verb 3+), output original content and new content
    if [[ $VERBOSE > 2 ]] && [[ $nc != '' ]]; then
      # add ' to start of original content for easier readability
      oc="'$oc"
      # Right justify (pad to the left) with two spaces beforehand
      printf "  %4s" "$oc"
      echo "' -> '$nc'"
    fi

    # Append new content to resulting string
    rstr+="$nc"

    # Account for newline characters separately for better step-by-step output
    if [[ "$c" == $'\n' ]]; then
      rstr+="$c"
      if [[ $VERBOSE > 2 ]]; then
        echo -e "    newline\n"
      fi
    fi
  done

  if [ $idx ]; then
    # One more append if there is a remaining index variable
    local row=${idx:0:1}
    local column=${idx:1:1}
    rstr+=$(get_square_item $row $column)
    # Report extra append if step-by-step
    if [[ $VERBOSE > 2 ]]; then
      oc+="'$idx"
      nc+=$(get_square_item $row $column)
      printf "  %4s" "$oc"
      echo "' -> '$nc'"
      echo    # Extra newline for spacing
    fi
  fi
}


# Playfair cipher (digram substitution cipher using Polybius square)
playfair () {
  # Playfair considers 'j' as 'i' and doesn't like spaces
  # Store substituted string in $wstr
  wstr="$(tr j i <<< ${istr//[[:space:]]/})"
  # Resulting string
  rstr=''

  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == Playfair Cipher tutorial =="
    echo
    echo "The Playfair cipher is a digram substitution cipher that uses"
    echo "a mixed alphabet and Polybius square to process two characters at once"
    echo
    malpha_tutor "$FKEY"
    psquare_tutor
    echo "Now that we have the Polybius square, we can perform the Playfair cipher:"
    echo "  1. Replace all j's in the input string with i"
    echo "  2. Loop through each pair of letters in the input string"
    echo "    a. If you encounter a pair of the same letter, add 'x' in between"
    echo "       ('falls' -> 'fa lx ls')"
    echo "    b. If you reach the end of the string with just one letter, add an extra 'x'"
    echo "       ('welcome' -> 'we lc om ex')"
    echo "  3. Locate the pair of letters in the Polybius square and find which rule it matches:"
    if [[ "$OP" == "de" ]]; then
      echo "    a. If they appear in the same colun, replace each letter with the one immediately above it"
      echo "       ('$(get_square_item 3 3)$(get_square_item 4 3)' -> '$(get_square_item 2 3)$(get_square_item 3 3)')"
      echo "    b. If they appear in the same row, replace each letter with the one immediately to the left"
      echo "       ('$(get_square_item 3 5)$(get_square_item 3 1)' -> '$(get_square_item 3 4)$(get_square_item 3 5)')"
    else
      echo "    a. If they appear in the same colun, replace each letter with the one immediately below it"
      echo "       ('$(get_square_item 2 3)$(get_square_item 3 3)' -> '$(get_square_item 3 3)$(get_square_item 4 3)')"
      echo "    b. If they appear in the same row, replace each letter with the one immediately to the right"
      echo "       ('$(get_square_item 3 4)$(get_square_item 3 5)' -> '$(get_square_item 3 5)$(get_square_item 3 1)')"
    fi
    echo "    c. If neither of those match, imagine a rectangle around them and select the other corners of it"
    echo "       Each letter should be replaced with the other corner that is on the same row"
    echo "       ('$(get_square_item 3 5)$(get_square_item 2 1)' -> '$(get_square_item 3 1)$(get_square_item 2 5)')"
    echo "  4. (opt.) The script doesn't do this because it is difficult to do programatically,"
    echo "     but you may wish to add spaces and special characters back in where they make sense"
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    echo

    # Print out 2d Polybius square
    print_square
    echo

    wait_enter
  fi

  # Using c-style loop to iterate over individual characters in $wstr
  for (( i=0; i<${#wstr}; i++ )); do 
    # Get current and next letters in $wstr
    cur=${wstr:$i:1}
    nxt=${wstr:$((i+1)):1}

    if [[ $cur =~ ^[[:alpha:]]$ ]]; then
      # If the next value is empty, it's the end of the string
      if [[ -z "$nxt" ]]; then
        # Pad with 'x'
        nxt=x
      # If the two characters are the same
      elif [[ "$cur" == "$nxt" ]]; then
        # Pad with 'x'
        nxt=x
      # If the next value is another alpha character, we will skip it next loop
      elif [[ $nxt =~ ^[[:alpha:]]$ ]]; then
        (( i++ ))
      else
        # If not a letter, we need to delete it from the string
        # Using sed at $i + 2 since 1 would be the first character
        # and we need to delete the next one
        wstr="$(echo $wstr | sed "s/.//$(( $i + 2 ))")"
        # Decrement i to attempt this pair again after deleting the character
        (( i-- ))
        # Continue with the loop
        continue
      fi

      if cur_array=($(get_square_indexes $cur)) && nxt_array=($(get_square_indexes $nxt)); then
        cur_row="${cur_array[0]}"
        cur_column="${cur_array[1]}"
        nxt_row="${nxt_array[0]}"
        nxt_column="${nxt_array[1]}"
        # New content added this loop run
        new_content=''

        # If they're in the same column
        if [[ $cur_column == $nxt_column ]]; then
          # Each character is "up" one row for deciphering (down for enciphering)
          if [[ "$OP" == "de" ]]; then
            new_content+="$(get_square_item $(( $cur_row - 1 )) $cur_column)$(get_square_item $(( $nxt_row - 1 )) $nxt_column)"
          else
            new_content+="$(get_square_item $(( $cur_row + 1 )) $cur_column)$(get_square_item $(( $nxt_row + 1 )) $nxt_column)"
          fi
        # If they're in the same row
        elif [[ $cur_row == $nxt_row ]]; then
          # Each character is to the left one column for deciphering (right for enciphering)
          if [[ "$OP" == "de" ]]; then
            new_content+="$(get_square_item $cur_row $(( $cur_column - 1 )))$(get_square_item $nxt_row $(( $nxt_column - 1 )))"
          else
            new_content+="$(get_square_item $cur_row $(( $cur_column + 1 )))$(get_square_item $nxt_row $(( $nxt_column + 1 )))"
          fi
        # If neither row nor column match between them
        else
          # Swap column values, same for (en/de)ciphering
          new_content+="$(get_square_item $cur_row $nxt_column)$(get_square_item $nxt_row $cur_column)"
        fi

        rstr+="$new_content "

        # Step-by-step commentary if sufficiently verbose
        if [[ $VERBOSE > 2 ]]; then
          echo "  '$cur$nxt' -> '$new_content'"
        fi
      else
        echo "Couldn't get index for $cur and/or $nxt" 1>&2
        continue
      fi
    else
      # If not a letter, we need to delete it from the string
      # Using sed at $i + 1 since 1 would be the first character 
      wstr="$(echo $wstr | sed "s/.//$(( $i + 1 ))")"
      # Decrement i to attempt this pair again after deleting the character
      (( i-- ))
    fi
  done

  if [[ $VERBOSE > 2 ]]; then echo; fi
}


# Bifid cipher (digraphic cipher using Polybius square like Playfair)
bifid () {
  # Bifid (like Playfair) considers 'j' as 'i'
  # Store substituted string in $wstr
  wstr="$(tr jJ iI <<< $istr)"
  # Resulting string
  rstr=''

  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == Bifid Cipher tutorial =="
    echo
    echo "The Bifid cipher is another somewhat complicated cipher that uses"
    echo "a mixed alphabet and Polybius square."
    echo "The cipher then intersperses ${bl}row${nm} (Y) and ${bl}column${nm} (X) values to mask the contents."
    echo
    malpha_tutor "$IKEY"
    psquare_tutor
    echo "Now that we have the Polybius square, we can start with the cipher by collecting row/column values:"
    echo " 1. For each letter in the input string, locate it in the Polybius square"
    if [[ "$OP" == "de" ]]; then
      echo " 2. Add the row and column values to a ${bl}single list${nm} of coordinates"
    else
      echo " 2. Add the row and column values to ${bl}separate lists${nm}"
    fi
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    echo

    # Print out 2d Polybius square
    print_square
    echo

    wait_enter "Create the list(s)"
  fi

  # Empty arrays for row and column values
  row_arr=( )
  col_arr=( )
  # Full array for rows and columns interspersed
  fullarr=( )

  # Using c-style loop to iterate over individual characters in $wstr
  for (( i=0; i<${#wstr}; i++ )); do 
    # Get current letter in wstr
    cur=${wstr:$i:1}

    # If it's an alphabetic character, get the index
    if [[ $cur =~ ^[[:alpha:]]$ ]]; then
      # Get current row/column position
      # Observe the layout of print_square() if this doesn't make sense
      cur_array=($(get_square_indexes $cur))
      cur_row="${cur_array[0]}"
      cur_column="${cur_array[1]}"
      # Append to arrays depending on operation
      if [[ "$OP" == "de" ]]; then
        fullarr+=( $cur_row $cur_column )
      else
        row_arr+=( $cur_row )
        col_arr+=( $cur_column )
      fi
    fi
  done

  if [[ "$OP" == "de" ]]; then
    arrlen=${#fullarr[@]}
    # Check and report potential problem if the full array length is odd
    if (( $arrlen % 2 )); then
      echo "bifid(): full array has an odd number of items; result may be incorrect" 1>&2
    fi

    if [[ $VERBOSE > 1 ]]; then
      echo "With the full list of coordinates, we need to split them up into row and column values:"
      echo " 1. Add the ${bl}first $PERIOD${nm} (current period value) numbers to the list of row values"
      echo " 2. Add the ${bl}next $PERIOD${nm} numbers to the list of column values"
      echo " 3. Repeat until you have less than ${bl}$(( $PERIOD * 2 ))${nm} values left"
      echo " 4. Add the ${bl}first half${nm} of the remaining numbers to the list of row values,"
      echo "    the ${bl}second half${nm} to the list of column values"
      echo
      
      # Right justify 10 spaces
      printf "%10s" "period"
      echo " = $PERIOD"
      printf "%10s" "coords"
      echo " = ${fullarr[@]}"
      echo

      wait_enter "Split the list"
    fi

    # Copy $PERIOD to another variable (interval) so we can edit it if needed
    # to account for string lengths that aren't a multiple of $PERIOD
    ivl=$PERIOD
    # Loop through every other multiple of $PERIOD;
    for i in $(seq 0 $(( $PERIOD * 2 )) $arrlen); do
      # Each run processes the next $PERIOD * 2 items.
      # We need to check if this will place it past the end of the array.
      # $diff is the number of items it will want to process this run
      # that do NOT have array elements corresponding to them.
      diff=$(( ($i + ($PERIOD * 2) - $arrlen) ))
      # If it is negative or zero, we're good.
      if (( $diff > 0 )); then
        # If it is positive, we need to alter the behavior so that
        # half of the remaining array elements are added as row- and column-values each.
        # For this run, the interval on which it operates needs to be
        # $diff / 2 less than the typical $PERIOD interval.
        ivl=$(( $PERIOD - ($diff / 2) ))
      fi
      # Add the next $ivl items as row values
      row_arr+=( ${fullarr[@]:$i:$ivl} )
      # Add the next $ivl items after that as column values
      col_arr+=( ${fullarr[@]:$(( $i + $ivl )):$ivl} )
    done

    if [[ $VERBOSE > 1 ]]; then
      echo "The two lists can now be read directly as row/column values for the result."
      echo
      printf "%10s" "rows"
      echo " = ${row_arr[@]}"
      printf "%10s" "columns"
      echo " = ${col_arr[@]}"
      echo
    fi
  else
    if [[ $VERBOSE > 1 ]]; then
      echo "With the lists of row and column values, we need to unity them into one list:"
      echo " 1. Add the first $PERIOD (current period value) ${bl}row${nm} values to the full list"
      echo " 2. Add the next $PERIOD ${bl}column${nm} values to the full list"
      echo " 3. ${bl}Repeat${nm} until all values have been added to the full list"
      echo
      # Right justify 10 spaces
      printf "%10s" "period"
      echo " = $PERIOD"
      printf "%10s" "rows"
      echo " = ${row_arr[@]}"
      printf "%10s" "columns"
      echo " = ${col_arr[@]}"
      echo

      wait_enter "Create the full list"
    fi

    # Loop through values from 0 to the volumn array length in $PERIOD increments
    for i in $(seq 0 $PERIOD ${#col_arr[@]}); do
      # Full array should be $PERIOD row values, then that many column values,
      # then the next $PERIOD rows and columns until the arrays are exhausted
      fullarr+=( ${row_arr[@]:$i:$PERIOD} ${col_arr[@]:$i:$PERIOD} )
    done

    if [[ $VERBOSE > 1 ]]; then
      echo "The full list that we now have is the list of row and column values for the result."
      echo "For example, the first result character is at row=${fullarr[0]}, col=${fullarr[1]}"
      echo
      printf "%10s" "coords"
      echo " = ${fullarr[@]}"
      echo
    fi
  fi

  if [[ $VERBOSE > 1 ]]; then
    echo "You may also want to refer back to the input string and add back"
    echo "spaces and special characters in the same positions in the resulting string."
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    echo

    # Print out 2d Polybius square
    print_square
    echo

    wait_enter
  fi

  # Array index
  ai=0

  # Looping through the original string again to simplify spaces/special chars
  for (( i=0; i<${#wstr}; i++ )); do
    # Get current letter from $wstr
    cur=${wstr:$i:1}

    nc=''
    # If it's an alphabetic character, add substitution character to result
    if [[ $cur =~ ^[[:alpha:]]$ ]]; then
      if [[ "$OP" == "de" ]]; then
        # Get row/column values depending on operation
        cur_row=${row_arr[$ai]}
        cur_column=${col_arr[$ai]}
        (( ai++ ))
      else
        # The row,column values for enciphering are the next two items in the full array
        cur_row=${fullarr[$ai]}
        cur_column=${fullarr[$(( ai + 1 ))]}
        # Increment index variable by 2
        (( ai+=2 ))
      fi
      nc=$(get_square_item $cur_row $cur_column)
      if [[ $VERBOSE > 2 ]]; then
        echo "  '$cur' -> '$nc' (r=$cur_row, c=$cur_column)"
      fi
    else
      # If not an alphabetic char, add whatever else it is to the result
      nc="$cur"
      if [[ $VERBOSE > 2 ]]; then
        if [[ "$cur" == $'\n' ]]; then
          echo -e "  newline\n"
        else
          echo "  '$cur' -> '$nc'"
        fi
      fi
    fi
    rstr+="$nc"
  done

  if [[ $VERBOSE > 2 ]]; then echo; fi
}


# Customized monoalphabetic substitution
substitute () {
  # Initialize wstr
  wstr="$istr"

  # Normal and substitution strings
  az=''
  sz=''
  # Variable for how to handle case for the substitution function
  # This determines the case of SUBSTITUTED letters; untouched letters will be the opposite case
  # Default to capitalizing input and replacing affected characters with lowercase
  subcase=C

  # Iterate over the substitution directives
  # Bash for loops do not recognize changes to the array while the loop is in progress.
  # So, unlike Python, we use two separate loops, 
  # once to process file and case directives,
  # and another to process substitution directives.
  # This will have some effects on edge cases detailed in the wiki.
  for arg in ${SUBARGS[@]}; do
    if [[ $arg =~ ^file= ]]; then
      # Grab filename as portion of argument after = sign
      fname=$(echo $arg | cut -d= -f2)
      if [[ $VERBOSE > 1 ]]; then echo "Will read from file: $fname"; fi

      # Read lines from the provided filename and add to SUBARGS
      # This will error but continue if the filename isn't valid
      while read l; do
        SUBARGS+=("$l")
      done < $fname
    elif [[ $arg =~ ^case= ]]; then
      # Attempt to canonicalize the provided case designator
      if ! subcase=$(canoncase $(echo $arg | cut -d= -f2)); then
        echo "Will use the default case" 1>&2
      fi
    elif [[ $arg =~ ^(src|from)= ]]; then
      newsrc=$(echo $arg | cut -d= -f2)
      az+=$newsrc
    elif [[ $arg =~ ^(dst|to)= ]]; then
      newdst=$(echo $arg | cut -d= -f2)
      sz+=$newdst
    fi
  done

  # Second time through the loop
  for arg in ${SUBARGS[@]}; do
    # Ignore any special directives on this pass
    if [[ $arg =~ ^(file|case|src|dst|from|to)= ]]; then
      continue
    # Verify that the first two characters are letters
    elif [[ $arg =~ ^[a-zA-Z]{2} ]]; then
      # Grab the first and second letters of the input
      # and add to the normal and substitution lists, respectively
      az+=${arg:0:1}
      sz+=${arg:1:1}
      if [[ $VERBOSE > 1 ]]; then echo "Will substitute $arg"; fi
    else
      echo "Ignoring unintelligible argument: $arg" 1>&2
    fi
  done

  # Compare lengths of the substitution strings
  # 'tr' in bash is fine with non-matching lengths,
  # but Python's translate isn't so it's truncated in both for consistency.
  if [[ ${#az} != ${#sz} ]]; then
    echo "Source and destination sets are not the same length;" 1>&2
    echo "will truncate to the shorter length" 1>&2
    az=${az:0:${#sz}}
    sz=${sz:0:${#az}}
  fi

  # Initialize various cases of the original and substitution lists
  azAZ=${az,,}${az^^}
  szsz=${sz,,}${sz,,}
  szSZ=${sz,,}${sz^^}
  SZSZ=${sz^^}${sz^^}
  # Variable for the selected sz version
  selsz=''

  # If subcase is none, then we will translate preserving case
  # This is NOT the default as it obfuscates which parts of the string were translated
  # and which still need analyzed to attempt a substitution
  if [[ $subcase == "N" ]]; then
    if [[ $VERBOSE > 1 ]]; then echo "Preserving original case"; fi
    selsz=$szSZ
  # If subcase is capital, then the input should be lowercase
  # and all substitutions should be uppercase
  elif [[ $subcase == "C" ]]; then
    if [[ $VERBOSE > 1 ]]; then echo "Replacements will be in uppercase"; fi
    wstr=${istr,,}
    selsz=$SZSZ
  # If subcase is capital then the input should be uppercase
  # and all substitutions should be lowercase
  elif [[ $subcase == "S" ]]; then
    if [[ $VERBOSE > 1 ]]; then echo "Replacements will be in lowercase"; fi
    wstr=${istr^^}
    selsz=$szsz
  fi

  # Perform substitution and check for errors
  if ! rstr="$(tr $azAZ $selsz <<< "$wstr")"; then
    echo "Unable to perform substitution; probably had no substitution directives." 1>&2
    usage noarg
  fi
}


# Generate a random monoalphabetic cipher operation
randomize () {
  rstr="$istr"

  # This will be the substitution alphabet
  rz=''

  # If the Caesar flag was provided, do a random Caesar offset
  if $CAE; then
    if [[ $VERBOSE > 1 ]]; then echo "Generating random Caesar offset"; fi
    # Generate random number between 1 and 25
    DIST=$((1 + $RANDOM % 25))
    # Always go right (although direction doesn't reall matter here)
    DIRECTION=R
    # Perform Caesar operation
    caesar
    # Set rz so the report knows what cipher operation was done
    rz="Caesar $DIST"
  # If no other recognized flags provided, do a fully random monoalphabetic substitution
  else
    if [[ $VERBOSE > 1 ]]; then echo "Generating random monoalphabetic substitution"; fi
    # Randomize normal alphabet list
    ralpha=( $(shuf -e "${alpha[@]}") )

    # Loop through regular alphabet to check for non-substituted letters.
    # The `shuf` command doesn't guarantee that every item will move from its original position.
    # However, for our purposes, we would like each alphabetic letter to
    # be substituted for a different letter.
    # This will accomplish that with one rare limitation:
    # if 'z' is not moved this will not be able to correct it.
    for c in ${alpha[@]}; do
      # Substitution character should be the first item in the
      # remaining randomized alphabet array
      sc=${ralpha[0]}
      # If the substitution letter is the same and there are enough available random letters,
      if [[ $c == $sc ]] && [[ ${#ralpha[@]} > 1 ]]; then
        if [[ $VERBOSE > 2 ]]; then echo "Same letter, looking ahead"; fi
        # use the next item in the array as the substitution letter instead
        sc=${ralpha[1]}
      fi
      # Add the substitution letter to the substitution alphabet string
      rz+=$sc
      if [[ $VERBOSE > 2 ]]; then echo "$c -> $sc"; fi
      # Remove the substitution character from the array
      ralpha=( ${ralpha[@]/$sc} )
    done

    if [[ $VERBOSE > 1 ]]; then
      echo "Finalized substitution alphabet: $rz"
    fi

    # Create uppercase substitution string
    RZ="${rz^^}"
    
    # Use tr to replace the randomized subsittution alphabet
    # in both lower and upper case.
    rstr="$(tr $az $rz <<< "$rstr")"
    rstr="$(tr $AZ $RZ <<< "$rstr")"
  fi
}


# Report current state of the resulting string, stripping spaces if requested
report () {
  local preface="$1"
  # Print preface if non-empty
  if [[ $VERBOSE > 0 ]] && [[ "$preface" != "" ]]; then
    echo "$preface"
  fi
  # Print result string with requested spacing and capitalization
  if $STRIP; then
    echo -e "$rstr" | tr $SET1 $SET2 | tr -d [[:blank:]]
  else
    echo -e "$rstr" | tr $SET1 $SET2
  fi
}


########## End of function declarations


########## Start of execution


#setup_start=$(now)
# initialize basic variables
init_vars

# Pass all command line arguments to get_args() for parsing
get_args "$@"

# Initialize strings
init_str
alphinit

#runtime $setup_start "script setup"

# Perform each cipher based on the contents of the input
# or confirmation to continue anyway

#ciphers_start=$(now)

for i in "${CIPHERS[@]}"; do
  # Determine contents of input string from the contains() function
  CONT=$(contains)
  # Split cipher operation into cipher name and parameter
  IFS="," read -r C_NAME C_PARAM <<< "$i"
  # Check for Vigenere naming so we can unify the branches
  if [[ "$C_NAME" == "ak" ]]; then
    AK=true
  else
    AK=false
  fi
  # Check what the cipher name is
  case $C_NAME in
    rand )
      if [[ "$C_PARAM" == "cae" ]]; then
        CAE=true
      else
        CAE=false
      fi
      randomize
      report "  Processed with ${bl}random monoalphabetic substitution${nm} ($rz):"
      ;;
    cae )
      DIST="$C_PARAM"
      if [ $CONT = "letters" ] || confirm; then 
        # Use Caesar Tutor if verbosity is 2+
        if [[ $VERBOSE > 1 ]]; then caesar_tutor; fi
        caesar
        # If brute force is set ('-b'), then output is provided in the function
        if ! $BR; then
          # Report the resulting string
          report "  Processed with ${bl}Caesar $DIRECTION $DIST${nm}:"
        fi
      fi
      ;;
    atb )
      if [[ $CONT == "letters" ]] || confirm; then 
        if [[ $VERBOSE > 1 ]]; then atbash_tutor; fi
        atbash
        report "  Processed with ${bl}Atbash${nm}:"
      fi
      ;;
    a1z ) 
      # No input validation as it will encipher letters and decipher numbers
      alphinit
      a1z26
      report "  Processed with ${bl}A1Z26${nm}:"
      ;;
    vig | ak )
      VKEY="$C_PARAM"
      if [ $CONT = "letters" ] || confirm; then 
        # Vigenere cipher should not be run with brute force mode
        # Running with BR would display each offset for each letter
        BR=false
        vigenere
        report "  Processed with ${bl}Vigenere $DIRECTION $VKEY${nm}:"
      fi
      ;;
    bea )
      BKEY="$C_PARAM"
      if [ $CONT = "letters" ] || confirm; then 
        alphinit b
        beaufort
        report "  Processed with ${bl}Beaufort $BKEY${nm}:"
      fi
      ;;
    kw )
      KKEY="$C_PARAM"
      if [ $CONT = "letters" ] || confirm; then 
        alphinit k $KKEY
        if [[ $VERBOSE > 1 ]]; then keyword_tutor; fi
        keyword
        report "  Processed with ${bl}Keyword $KKEY${nm}:"
      fi
      ;;
    ply )
      PKEY="$C_PARAM"
      # No input validation as it will encipher letters and decipher numbers
      alphinit p $PKEY
      polybius
      report "  Processed with ${bl}Polybius $PKEY${nm}:"
      ;; 
    pf )
      FKEY="$C_PARAM"
      if [ $CONT = "letters" ] || confirm; then 
        alphinit p $FKEY
        playfair
        report "  Processed with ${bl}Playfair $FKEY${nm}:"
      fi
      ;;
    bif )
      IKEY="$C_PARAM"
      if [ $CONT = "letters" ] || confirm; then 
        alphinit p $IKEY
        bifid
        report "  Processed with ${bl}Bifid $IKEY $PERIOD${nm}:"
      fi
      ;;
    sub )
      # Verify that we've gotten some arguments describing how to substitute
      if [ ${#SUBARGS[@]} -eq 0 ]; then
        echo "Need at least one argument for custom substitutions." 1>&2
        echo "Got ${SUBARGS[@]}" 1>&2
        usage noarg
      fi
      if [ $CONT = "letters" ] || confirm; then 
        if [[ $VERBOSE > 1 ]]; then echo ${SUBARGS[@]}; fi
        substitute
        report "  Processed with custom monoalphabetic substitution:"
      fi
      ;;
  esac
  # Account for series operation
  if $SEQUENTIAL; then
    istr="$rstr"
  fi
done

#runtime $ciphers_start "cipher operation(s)"
