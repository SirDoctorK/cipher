#!/usr/bin/python3


# Ciphertext analysis script


########## Imports


import argparse, os, sys, string, platform
from collections import defaultdict
from math import isclose
from itertools import zip_longest
try:
  from cipher import Logger, Color
except ImportError as e:
  print("This script requires supporting classes from 'cipher.py'; check that it is present and accessible.", file=sys.stderr)
  raise e


########## Classes


class TextSample:
  alphabet = list(string.ascii_lowercase)

  def __init__(self, name: str) -> None:
    self.name = name

  def read_from_text(self, input: str) -> None:
    Logger.info("Analysing letter frequency")
    # lowercase working string for simplicity
    input = input.lower()

    # Variable for count of letters in string
    letters = 0
    # Initalize dict of alphabetic letters with their count in the input
    self.letter_counts = dict.fromkeys(self.alphabet, 0)

    # Initialize dict for letter pair counts
    # Using defaultdict so uninitialized keys are assumed to have 0 value
    self.pair_counts = defaultdict(int)
    # Another for doubles of the same letter
    self.double_counts = defaultdict(int)

    # Loop through individual characters in input.
    # Using for loop so we can look at both current and next characters
    for i in range(len(input)-1):
      # Get current and next letters in input
      try:
        cur = input[i]
      except:
        # If can't get cur, just increment i and go to next loop run
        i += 1
        continue
      try:
        nxt = input[i + 1]
      except:
        # If can't get nxt, probably end of string
        # For end of string, nxt should be blank (to be padded with 'x') and continue with that
        nxt = ''

      if cur.isalpha():
        # If it's an alphabetic char, increment the letter count
        # and the count for the current letter
        letters += 1
        self.letter_counts[cur] += 1
        # Also check if nxt is alphabetic
        if nxt.isalpha():
          pair = cur + nxt
          # Increment the appropriate pair of letters
          # Due to defaultdict, don't need to check if the key exists or not
          if cur == nxt:
            # If equal, it's a double
            self.double_counts[pair] += 1
          else:
            # Otherwise it's a regular pair
            self.pair_counts[pair] += 1
    
    # Convert counts to frequencies and save to self
    self.letter_frequencies = self.count_to_frequency(self.letter_counts)
    self.pair_frequencies = self.count_to_frequency(self.pair_counts)
    self.double_frequencies = self.count_to_frequency(self.double_counts)
  
  @staticmethod
  def count_to_frequency(counts: dict) -> dict:
    total_count = sum(counts.values())
    frequencies = {letter: round(((count / total_count) * 100), 2) for letter, count in counts.items()}
    return frequencies


class English(TextSample):
  # Typical frequencies of English letters
  # from http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
  letter_frequencies = {
    'a': 8.12,
    'b': 1.49,
    'c': 2.71,
    'd': 4.32,
    'e': 12.02,
    'f': 2.30,
    'g': 2.03,
    'h': 5.92,
    'i': 7.31,
    'j': 0.10,
    'k': 0.69,
    'l': 3.98,
    'm': 2.61,
    'n': 6.95,
    'o': 7.68,
    'p': 1.82,
    'q': 0.11,
    'r': 6.02,
    's': 6.28,
    't': 9.10,
    'u': 2.88,
    'v': 1.11,
    'w': 2.09,
    'x': 0.17,
    'y': 2.11,
    'z': 0.07
  }
  # from https://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/digraphs.html
  pair_frequencies = {
    'th':	1.52,
    'he':	1.28,
    'in':	0.94,
    'er':	0.94,
    'an':	0.82,
    're':	0.68,
    'nd':	0.63,
    'at':	0.59,
    'on':	0.57,
    'nt':	0.56,
    'ha':	0.56,
    'es':	0.56,
    'st':	0.55,
    'en':	0.55,
    'ed':	0.53,
    'to':	0.52,
    'it':	0.50,
    'ou':	0.50,
    'ea':	0.47,
    'hi':	0.46,
    'is':	0.46,
    'or':	0.43,
    'ti':	0.34,
    'as':	0.33,
    'te':	0.27,
    'et':	0.19,
    'ng':	0.18,
    'of':	0.16,
    'al':	0.09,
    'de':	0.09,
    'se':	0.08,
    'le':	0.08,
    'sa':	0.06,
    'si':	0.05,
    'ar':	0.04,
    've':	0.04,
    'ra':	0.04,
    'ld':	0.02,
    'ur':	0.02
  }
  # from http://homepages.math.uic.edu/~leon/mcs425-s08/handouts/doublechar_freq.pdf
  double_counts = {
    'aa': 1,
    'bb': 1,
    'cc': 4,
    'dd': 13,
    'ee': 48,
    'ff': 11,
    'gg': 4,
    'hh': 6,
    'ii': 1,
    'jj': 0,
    'kk': 0,
    'll': 56,
    'mm': 5,
    'nn': 8,
    'oo': 36,
    'pp': 10,
    'qq': 0,
    'rr': 14,
    'ss': 43,
    'tt': 56,
    'uu': 0,
    'vv': 0,
    'ww': 2,
    'xx': 0,
    'yy': 2,
    'zz': 0
  }
  # Convert to frequency
  double_frequencies = TextSample.count_to_frequency(double_counts)
  
  def __init__(self, name="Typical English") -> None:
    super().__init__(name)


class Comparator:
  class Margins:
    coarse = 0.6
    precise = 0.4

  def __init__(self, reference: TextSample, subject: TextSample, graph_width=None) -> None:
    self.reference = reference
    self.subject = subject
    self.subject.num_letters = sum(self.subject.letter_counts.values())
    # Set column widths
    if graph_width:
      self.graph_width = int(graph_width)
    else:
      try:
        self.graph_width, lines = os.get_terminal_size()
      except:
        # If unable to get size, default to 80 columns
        self.graph_width = 80
    # Set bars to 10 less than the total width of the terminal
    self.bar_width = self.graph_width - 10
  
  @staticmethod
  def format_tuple(input) -> str:
    # Recognize tuples with multiple items
    if isinstance(input, tuple) and len(input) > 1:
      return f"{input[0]}: {input[1]}"
    # Regularly print other types
    else:
      return str(input)
  
  @staticmethod
  def get_top_items(num: int, iter1, iter2='') -> str:
    # Initialize the variables that will be printed
    sorted1 = iter1
    sorted2 = iter2

    # Handle dicts in both possible positions
    # Turns a dict into a list of tuples
    if isinstance(iter1, dict):
      # Sort by the value of the dict in reverse order (larger to smaller)
      sorted1 = sorted(iter1.items(), key=lambda item: item[1], reverse=True)
    if isinstance(iter2, dict):
      sorted2 = sorted(iter2.items(), key=lambda item: item[1], reverse=True)

    result = ""
    
    # Loop through values for both sorted lists
    # Using 'zip_longest()' to iterate through both until the *longer* list is exhausted
    # Setting the 'fillvalue' so that once one is exhausted it's just empty
    for v1, v2 in zip_longest(sorted1[:num], sorted2[:num], fillvalue=''):
      # Use 'format_tuple()' to format any tuples that might appear in the loop
      # and using 'ljust' to make the second set appear in a consistent position
      # and to have it line up with the titles in the lists function
      result += f"{Comparator.format_tuple(v1).ljust(18)}{Comparator.format_tuple(v2)}\n"
    
    return result

  def get_frequency_graph(self, freq: dict, maxval: float) -> str:
    result = ""
    # Scale according to maximum value
    scale = self.bar_width / maxval

    for k, v in freq.items():
      bar_len = round(v * scale)
      # Use string multiplication to construct a string with
      # the correct amount of fill character and space
      bar = ("█" * bar_len).ljust(self.bar_width)
      result += "{} {:>5} |{}|\n".format(k, v, bar)
    return result

  @staticmethod
  def close_enough(n1: float, n2: float, margin: float=Margins.coarse) -> bool:
    Logger.tutorial(f"Comparing {n1} and {n2} with margin {margin}")
    # The math.isclose() function allows specifying relative or absolute tolerances or both
    # However, we've got them split up here so that we can tell which tolerance was matched
    rel = abs = False
    # Test with relative tolerance (relative to the larger number)
    if isclose(n1, n2, rel_tol=margin):
      rel = True
      Logger.tutorial("  Relatively close")
    # Test with absolute tolerance
    if isclose(n1, n2, abs_tol=margin):
      abs = True
      Logger.tutorial("  Absolutely close")

    # Return either boolean if one is true
    return rel or abs

  # Show graph of letter frequencies
  def show_graphs(self) -> None:
    max_ref = max(self.reference.letter_frequencies.values())
    max_sub = max(self.subject.letter_frequencies.values())
    max_all = max(max_ref, max_sub)

    Logger.info(f"Letter count: {self.subject.num_letters}")
    Logger.info(f"Max frequency: {max_all}")

    Logger.normal(f"\n == {self.reference.name} distribution:")
    Logger.normal(self.get_frequency_graph(self.reference.letter_frequencies, max_all))
    
    Logger.normal(f"\n == {self.subject.name} distribution (of {self.subject.num_letters} letters):")
    Logger.normal(self.get_frequency_graph(self.subject.letter_frequencies, max_all))

  # Show lists of common English and Input letters, pairs, and doubles
  def show_lists(self, num_items: int) -> None:
    Logger.normal()
    Logger.normal(f" == Top {num_items} single letters:")
    Logger.normal(f"{self.reference.name} / {self.subject.name} (of {self.subject.num_letters} letters)")
    Logger.normal(self.get_top_items(num_items, self.reference.letter_frequencies, self.subject.letter_frequencies))

    Logger.normal(f" == Top {num_items} letter pairs:")
    Logger.normal(f"{self.reference.name} / {self.subject.name}")
    Logger.normal(self.get_top_items(num_items, self.reference.pair_frequencies, self.subject.pair_frequencies))

    Logger.normal(f" == Top {num_items} letter doubles:")
    Logger.normal(f"{self.reference.name} / {self.subject.name}")
    Logger.normal(self.get_top_items(num_items, self.reference.double_frequencies, self.subject.double_frequencies))
  
  # Look for signs of a Caesar cipher
  def like_caesar(self) -> None:
    # Warn of imprecision if less than 100 letters
    if self.subject.num_letters < 100:
      Logger.error(f"  {self.subject.name} too short to provide accurate Caesar inferences.")
    # Lists for low-, med-, and high-confidence suspected offsets
    offset_low = []
    offset_med = []
    offset_high = []
    # Copy alphabet
    alpha = TextSample.alphabet

    # Loop through all alphabetic letters with indices
    # This low confidence loop looks for frequencies that might be like 'x' and 'z';
    # basically, two letters with low frequencies separated by one other letter ('y')
    for i, l in enumerate(alpha):
      Logger.stepwise(f"Looking at {l} as x . . .")
      # Check if the input string frequency of l is close to 'x'
      if self.close_enough(
            self.reference.letter_frequencies['x'], 
            self.subject.letter_frequencies[l], 
            self.Margins.coarse):
        # If it is check if l+2 is close to 'z'
        if self.close_enough(
              self.reference.letter_frequencies['z'], 
              self.subject.letter_frequencies[alpha[(i+2) % len(alpha)]], 
              self.Margins.coarse):
          o = (i+3) % 26
          Logger.stepwise(f"  Looks similar (offset {o}).")
          offset_low.append(o)
    
    # If no low confidence offsets, it doesn't look like a Caesar cipher
    if not offset_low: Logger.normal("Doesn't look like a Caesar cipher.")

    # Sort low confidence offsets
    offset_low.sort()

    # Loop through low confidence offsets to see which can be upgraded
    for o in offset_low:
      Logger.tutorial(f"Closer look at offset {o} . . .")
      # This counter tracks each time the letter frequencies aren't close enough.
      misses = 0
      for i, l in enumerate(alpha):
        # il is the input letter correlating to l based on offset o
        il = alpha[(i+o) % len(alpha)]
        Logger.stepwise(f"Comparing {l} to {il}")
        if not self.close_enough(
              self.reference.letter_frequencies[l], 
              self.subject.letter_frequencies[il], 
              self.Margins.precise):
          # If it isn't close enough, log a miss
          Logger.tutorial(f"  Miss: {self.reference.name} {l} and {self.subject.name} {il} are not close enough.")
          misses += 1
      
      # If no misses, the offset is given High confidence
      if misses == 0:
        Logger.tutorial("  Wow, looks very likely; upgrading to High confidence.")
        offset_low.remove(o)
        offset_high.append(o)
      # If less than 5 misses, the offset is given Medium confidence
      elif misses < 5:
        Logger.tutorial("  Hmm, looks plausible; upgrading to Medium confidence.")
        offset_low.remove(o)
        offset_med.append(o)
      # Otherwise, left at Low confidence
      else:
        Logger.tutorial(f"  Nope, too many misses ({misses}); still Low confidence")

    # If there are Low confidence offsets, report them
    if offset_low:
      # Collect offsets on one-line string
      offset_msg = "Low confidence Caesar offsets:"
      for o in offset_low:
        offset_msg += f" {o}"
      # Print collected offset information
      Logger.normal(offset_msg)

    # Same thing for Medium confidence
    if offset_med:
      offset_msg = "Medium confidence Caesar offsets:"
      for o in offset_med:
        offset_msg += f" {o}"
      Logger.normal(offset_msg)

    # Same thing for High confidence
    if offset_high:
      offset_msg = "High confidence Caesar offsets:"
      for o in offset_high:
        offset_msg += f" {o}"
      Logger.normal(offset_msg)


########## Functions


# Function to get args for more easy navigation of the code
def get_args(custom_args=None) -> argparse.Namespace:
  # Initialize parser 
  # No built-in help so we can add it separately to the "Script modifiers" group
  # Using alternate formatter so the longer description can be used
  # Setting description and epilog for usage synopsis
  parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter, 
  description='''
  Cipher Analysis script
  Written by Stephen Kendall

  Provides useful analysis information for a ciphertext-only attack.
  So far, intended for use with monoalphabetic subsitution ciphers.''', 
  epilog="Consult the Wiki for more details: https://gitlab.com/SirDoctorK/cipher/-/wikis/home")

  # Default action for arguments is "store", defined explicitly for readability
  parser.add_argument("-i", "--input", dest="file", action="store", 
      help="file containing text to process \nif this flag is not set, it will read from standard input through redirection or keyboard input")
  parser.add_argument("-v", "--verbose", action="count", default=0, 
      help="enable more detailed output")
  parser.add_argument("-w", "--width", action="store", 
      help="set the width (in characters) of the graphs (defaults to terminal width or 80)")
  
  parser.add_argument("-G", "--graph", action="store_true",
      help="show graph of letter frequencies")
  # Using 'const=' instead of default as this works with 'nargs="?"',
  # meaning that the const is set only if the flag is provided AND no argument is provided
  parser.add_argument("-L", "--list", action="store", type=int, const=5, nargs="?",
      help="show list of top N letters, pairs, and doubles")
  parser.add_argument("-C", "--caesar", action="store_true",
      help="analyse input as possible Caesar cipher and look for likely offset distances")
  
  return parser.parse_args(custom_args)


# Initialize starting string from input file or stdin
def init_str(input_file="", input_string="") -> str:
  if input_file:
    Logger.info(f"Reading from {Color.highlight(input_file)}")
    input = open(input_file, "r").read()
  elif input_string:
    input = input_string
  # If no filename, reads from stdin with same behavior as 'cat' in bash edition
  else:
    # String needs to be initialized before it can be appended to
    input = ''
    if platform.system() == 'Windows':
      Logger.info(f"No filename, reading from stdin ('{Color.highlight('Ctrl-Z, Enter')}' to detach if interactive)")
    else:
      Logger.info(f"No filename, reading from stdin ('{Color.highlight('Ctrl-D')}' to detach if interactive)")
    for line in iter(sys.stdin.readline, ''):
      input += line
  # Strip leading and trailing newlines from input string (bash does this automatically)
  input = input.strip()
  # Exit script if the input string is empty
  if input == '':
    raise ValueError("Empty input string; nothing to do.")
    #sys.exit(1)
  return input


########## Main execution


# Main function when run as script
def main(custom_args=None, input_string="") -> int:
  try:
    args = get_args(custom_args)
    if not args.graph and not args.list and not args.caesar:
      print("No output requested. Add '-G', '-L', and/or '-C' to specify what reports to print.")
    Logger.set_print_level(args.verbose)
    input = init_str(args.file, input_string)
  except Exception as e:
    # Exceptions should be turned into a return value for the function 
    # so they don't mess up the Flask app ('cipherApp.py')
    Logger.error(f"{type(e).__name__}: {e}")
    return 1

  english = English()

  provided_text = TextSample(name="Input string")
  provided_text.read_from_text(input=input)

  analysis = Comparator(reference=english, subject=provided_text, graph_width=args.width)

  if args.graph: 
    analysis.show_graphs()

  if args.list: 
    analysis.show_lists(args.list)

  if analysis.subject.num_letters < 100:
    Logger.error("  Input is shorter than 100 letters; results are likely to be imprecise.")

  if args.caesar: 
    analysis.like_caesar()
  
  return 0


# Only call main if run as script (and pass return value of main)
if __name__ == "__main__":
  sys.exit(main())
