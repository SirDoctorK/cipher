# Cipher

* `cipher.py` - Python script for deciphering and enciphering text
* `cipher.sh` - Original Bash edition with the same functionality
* `analyze.py` - Analyzes ciphertexts for cracking monoalphabetic ciphers
* `cipherApp.py` - Flask app to provide a web interface for using the Cipher script

Capable of deciphering and enciphering with Caesar, Atbash, A1Z26, Vigenère, Beaufort, Keyword, Playfair, and Bifid.

The Vigenère implementation supports the Beaufort, Gronsfeld, and Autokey variations.

Inspired by the ciphers seen in [Gravity Falls](https://gravityfalls.fandom.com/wiki/List_of_cryptograms/Episodes) and including sample text files from end credits scenes.

Detailed documentation is now in the [Project Wiki](https://gitlab.com/SirDoctorK/cipher/-/wikis/home).