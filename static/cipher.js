// Declarations


var cipher_number = 0;
const keyed_ciphers = ["vigenere", "autokey", "beaufort", "keyword", "polybius", "playfair", "bifid", "substitute"];
const cipher_form = document.getElementById("cipher-form");
const analyze_form = document.getElementById("analyze-form");
const cipher_operations = document.getElementById('cipher-operations');
const toggleSwitch = document.getElementById('dark-theme');
const resultID = "result-blocks";
const savedTheme = localStorage.getItem('theme') ? localStorage.getItem('theme') : null;


// Functions


function openTab(evt, tabID) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(tabID).style.display = "block";
  evt.currentTarget.className += " active";
}


function addCipher() {
  // from https://www.quirksmode.org/dom/domform.html
  cipher_number++;
  var newField = document.getElementById('cipher-template').cloneNode(true);
  newField.id = 'cipher' + cipher_number;
  newField.style.display = 'block';

  var newFields = newField.children;
  // console.log(newFields);
  for (var i = 0; i < newFields.length; i++) {
    var originalName = newFields[i].id
    // console.log(originalName);
    if (originalName) {
      var newName = originalName + cipher_number;
      // console.log(newName);
      newFields[i].name = newName;
      newFields[i].id = newName;
    }
  }

  cipher_operations.append(newField);
}


function removeElement(elem) {
  elem.remove();
}


function resetCiphers() {
  var descendants = cipher_operations.childNodes;
  // console.log(descendants);
  var i, e;
  for (i = 0; i < descendants.length; ) {
    // console.log(descendants.length);
    e = descendants[i];
    if (e.id != "cipher-template") {
      removeElement(e);
    }
    else { // only increment index if the element was not subject to removal
      i++;
    }
  }
  addCipher();
}


function hideElement(elem) {
  elem.style.display = "none";
}


function showElement(elem) {
  elem.style.display = "block";
}


function showCipherParams(sel){
  // from https://stackoverflow.com/questions/15566999/how-to-show-form-input-fields-based-on-select-value
  var selection = sel.value;
  var parent = sel.parentNode;
  var target_key = parent.querySelector('.cipher-key')
  var target_offset = parent.querySelector('.cipher-offset')
  var target_random = parent.querySelector('.random-cipher-hint')
  if (keyed_ciphers.includes(selection)) {
    showElement(target_key)
    hideElement(target_offset)
    hideElement(target_random)
  } else if (selection == "caesar") {
    showElement(target_offset)
    hideElement(target_key)
    hideElement(target_random)
  } else if (selection == "random") {
    showElement(target_random)
    hideElement(target_key)
    hideElement(target_offset)
  } else {
    hideElement(target_key)
    hideElement(target_offset)
    hideElement(target_random)
  }
}


function getTextWidth(text, font) {
  // from https://stackoverflow.com/questions/68574895/measure-text-width-according-to-css-without-rendering-it
  // re-use canvas object for better performance
  var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
  var context = canvas.getContext("2d");
  context.font = font;
  var metrics = context.measureText(text);
  console.log("Getting text witdth with style: ", font)
  return metrics.width;
}


function calculateCharacterWidth(elementId) {
  // Heavily modified from suggestion by ChatGPT
  const codeBlock = document.getElementById(elementId);
  
  // Get the window width minus one to ensure it doesn't go too long
  const windowWidth = codeBlock.offsetWidth - 1;
  
  // Locate style of actual code block (https://stackoverflow.com/questions/14275304/how-to-get-margin-value-of-a-div-in-plain-javascript)
  const codeStyle = codeBlock.currentStyle || window.getComputedStyle(codeBlock);
  const codeFont = codeStyle.font;
  const actualCharacterWidth = getTextWidth("a", codeFont);
  console.log("actualTextWidth: ", actualCharacterWidth);
  
  const sidePadding = parseInt(codeStyle.paddingLeft) + parseInt(codeStyle.paddingRight);
  const usableWidth = windowWidth - sidePadding;

  // Calculate the character width in pixels
  const zoomLevel = window.devicePixelRatio || 1; // Account for zoom
  const characterWidth = actualCharacterWidth / zoomLevel;
  
  // Calculate the maximum number of characters that can fit
  const maxCharacters = Math.floor(usableWidth / characterWidth);

  console.log("Character width factors: ", windowWidth, sidePadding, zoomLevel, characterWidth)
  
  return maxCharacters;
}

function switchTheme(e) {
  // dark theme functionality from https://dev.to/ananyaneogi/create-a-dark-light-mode-switch-with-css-variables-34l8
  if (e.target.checked) {
      document.documentElement.setAttribute('data-theme', 'dark');
      localStorage.setItem('theme', 'dark');
  }
  else {
      document.documentElement.setAttribute('data-theme', 'light');
      localStorage.setItem('theme', 'light');
  }    
}


// Event listeners


analyze_form.addEventListener("submit", (e) => {
  e.preventDefault();

  let resultWidth = calculateCharacterWidth("std-out");
  console.log("result width: ", resultWidth);

  let formData = new FormData(analyze_form);
  formData.append("width", resultWidth);

  fetch("submit-analyzer", {
    method: "POST",
    body: formData
  })
  .then(response => response.text()) // Assuming the response is text content
  .then(data => {
    const resultElement = document.getElementById("result-blocks");
    resultElement.innerHTML = data; // Update the result div with the returned content
  })
  .catch(error => {
    console.error("Error:", error);
  });
});


cipher_form.addEventListener("submit", (e) => {
  e.preventDefault();

  let formData = new FormData(cipher_form);

  fetch("submit-cipher", {
    method: "POST",
    body: formData
  })
  .then(response => response.text()) // Assuming the response is text content
  .then(data => {
    const resultElement = document.getElementById("result-blocks");
    resultElement.innerHTML = data; // Update the result div with the returned content
  })
  .catch(error => {
    console.error("Error:", error);
  });
});


cipher_form.addEventListener("reset", resetCiphers);


toggleSwitch.addEventListener('change', switchTheme, false);


// Main execution


window.onload = addCipher;

document.getElementById("defaultTab").click();

if (savedTheme) {
  document.documentElement.setAttribute('data-theme', savedTheme);

  if (savedTheme === 'dark') {
      toggleSwitch.checked = true;
  }
}
