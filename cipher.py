#!/usr/bin/python3


########## Imports


import argparse, sys, os, re, string, platform, random
from argparse import ArgumentParser, Namespace
from urllib.request import Request, urlopen
from urllib.error import URLError
from datetime import datetime
from enum import Enum, auto
from functools import total_ordering
from typing import Iterable
from textwrap import dedent, indent


########## Global defaults


# For generating input text
GENERATION_URL = "https://randomword.com/paragraph"
GENERATION_DIV_ID = "random_word_definition"


########## Classes


##### Supporting Classes


# Smart enum class with member canonicalization and dual representations
class SmartEnum(Enum):
  """
  SmartEnum: provides member canonicalization and dual representations
  
  SmartEnum is a base class for creating custom enumerations with additional features.
  It allows initializing new enums with both a value and a string representation,
  provides string conversion support, and offers a canonicalization method for converting
  values or strings to the corresponding enum member.

  Usage:
    - Inherit from SmartEnum to create custom enumerations with value and string representation
    - Create members in your class with integer and string representations (use auto)
    - The `__str__` method returns the string representation of the enum
    - The `canonicalize` class method converts members, values, or strings to the corresponding enum member
  
  Example:
    ```
    class MyEnum(SmartEnum):
      ONE = 1, "One"
      TWO = 2, "Two"
      WHATEVER = auto(), "Whatever"
    
    my_value1 = MyEnum.canonicalize("One")
    my_value2 = MyEnum.canonicalize(2)
    ```
  """
  # Initialize new enums with value *and* string representation
  def __new__(cls, value, string_repr) -> Enum:
    obj = object.__new__(cls)
    obj._value_ = value
    obj.string_repr = string_repr
    return obj

  # Catch string conversions of enum
  def __str__(self) -> str:
    return self.string_repr

  # Canonicalize from value or string input
  @classmethod
  def canonicalize(cls, input) -> Enum:
    """
    Canonicalize the input value or string to the corresponding enum member.

    Args:
        input: The value to be canonicalized
            This may be an int, str, or member of the class

    Returns:
        Enum: The corresponding enum member

    Raises:
        TypeError: If the input is not of a valid type
        ValueError: If the input is a valid type but no matching enum member is found
    """
    input_type = type(input)
    # If it is already of the correct type, return it back
    if input_type == cls:
      return input
    elif input_type == int:
      for member in cls:
        if member.value == input:
            return member
    elif input_type == str:
      for member in cls:
        if str(member) == input:
            return member
    else:
      raise TypeError(f"Cannot canonicalize {cls} from {input_type}")
    # If we get to this point then we tried and failed to find a matching value
    raise ValueError(f"No member in {cls} matching {input}")
  

# Class for expanded printing capabilities
class Logger:
  # Enum form of levels, with both string and int representations
  @total_ordering
  class Level(SmartEnum):
    DISABLED = -50, "disabled"
    QUIET = -1, "quiet"
    NORMAL = 0, "normal"
    INFO = 1, "info"
    TUTORIAL = 2, "tutorial"
    STEPWISE = 3, "stepwise"

    # Canonicalize from value or string input, default to highest member if not found
    @classmethod
    def canonicalize(cls, input) -> Enum:
      try:
        return super().canonicalize(input)
      except ValueError:
        highest_member = max(cls, key=lambda member: member.value)
        return highest_member
    
    # Allow comparisons between these enums (only this class needs comparisons)
    def __lt__(self, other) -> bool:
      if self.__class__ is other.__class__:
        return self.value < other.value
      return NotImplemented
    
  # Globabl verbosity level defaults
  print_level = Level.NORMAL
  # Having this higher means additional code will execute which doesn't seem to impact runtime
  file_level = Level.STEPWISE
  # Find directory of script
  script_loc = os.path.dirname(os.path.realpath(__file__))
  # Create a file in that path in an os-agnostic way
  file_name = os.path.join(script_loc, "cipher.log")
  # Open that file path for writing (overwrites existing content)
  try:
    file_obj = open(file_name, 'w')
  # If it can't open the file, it will silently open dev null instead
  # to operate as a trash bin for anything written to it.
  except:
    file_obj = open(os.devnull, 'w')
  # Regex for finding color codes
  ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]')

  @classmethod
  def get_higher_level(cls) -> Enum:
    return max(cls.print_level, cls.file_level)

  @classmethod
  def set_print_level(cls, new_level) -> None:
    cls.print_level = cls.Level.canonicalize(new_level)

  # Method to print based on globabl verbosity
  @classmethod
  def output(cls, message_level=Level.NORMAL, message='') -> None:
    # Greater than or equal allows the checks to be a little more intuitive;
    # the message level passed to the function should represent
    # the verbosity level at which the provided args should be passed to print().
    if cls.print_level >= message_level:
      print(message)
    # All things printed to console will be added to the file,
    # plus all things included in the file_level
    if cls.print_level >= message_level or cls.file_level >= message_level:
      try:
        cls.output_file(str(message_level), message)
      except UnicodeError as e:
        print(f"{type(e).__name__}: {e}", file=sys.stderr)
  
  # Method to print to file object
  @classmethod
  def output_file(cls, level_desc: str, message=''):
    # Remove escape sequences from message
    plain_message = cls.ansi_escape.sub('', str(message))
    # Construct prefix by getting date and level description
    prefix = f"{datetime.now()}{level_desc.rjust(10)}: "
    # Print line by line so multiline strings each have prefixes
    # (this also seems to cause it to ignore empty messages)
    for line in plain_message.splitlines():
      print(prefix + line, file=cls.file_obj)

  # Error printing method
  @classmethod
  def error(cls, message) -> None:
    print(message, file=sys.stderr)
    cls.output_file("ERROR", message)
  
  # Wrappers for output() to simplify level specifications
  @classmethod
  def normal(cls, message='') -> None:
    return cls.output(cls.Level.NORMAL, message)
  
  @classmethod
  def info(cls, message='') -> None:
    return cls.output(cls.Level.INFO, message)
  
  @classmethod
  def tutorial(cls, message='') -> None:
    return cls.output(cls.Level.TUTORIAL, message)
  
  @classmethod
  def stepwise(cls, message='') -> None:
    return cls.output(cls.Level.STEPWISE, message)


# Simple class for color codes
class Color:
  # General color codes
  bold = '\033[1m'
  clear = '\033[0m'

  # Highlight (bold) the provided content
  @classmethod
  def highlight(cls, content: str) -> str:
    # Return highlighted string
    return cls.bold + str(content) + cls.clear

  # Clear color codes for situations where no color should be used
  @classmethod
  def clear_codes(cls) -> None:
    cls.bold = cls.clear = ''


# Class for calculating runtime of code chunks
class RunTime:
  # Class attribute to control whether to compute and print runtime
  # (this way it can easily be controlled with a flag to the script)
  enabled = False
  def __init__(self, desc="unspecified task", full_output=True) -> None:
    if self.enabled:
      self.starttime = datetime.now()
      self.desc = desc
      self.full_output = full_output
      # Initialize these attributes to None for now
      # so that we don't get AttributeError if they're referenced before they're set.
      self.endtime = self.elapsed = self.lastend = None
  
  # String method works even if called before end() is called
  def __str__(self) -> str:
    if self.enabled:
      if self.elapsed: 
        if self.full_output: 
          if self.lastend: return f"    - {self.desc} took {self.elapsed} ({self.endtime - self.lastend} since last end)"
          else: return f"    - {self.desc} took {self.elapsed}"
        else: return f"{self.elapsed.seconds}.{str(self.elapsed.microseconds).rjust(6, '0')}"
      else: return f"    - {self.desc} started at {self.starttime} and hasn't yet finished"
  
  def end(self) -> None:
    if self.enabled:
      # If there is already an end, keep it around as a comparison point
      if self.endtime: self.lastend = self.endtime
      # Set end time and calculate delta
      self.endtime = datetime.now()
      self.elapsed = self.endtime - self.starttime
      print(self)


##### Core classes


### Generic Ciphers


class Cipher:
  """Generic Cipher base class

  Provides a framework from which to implement text-based ciphers.
  It provides numerous methods and attributes that will generally apply to
  all specific ciphers. It also provides a few example methods that should be
  overridden in child classes.

  Enums:
    - Case*: NONE, ORIGINAL, LOWER, UPPER
    - Operation*: ENCIPHER, DECIPHER
    - InputType: UNKNOWN, DIGITS, LETTERS
    - Assumption*: NONE, NO, YES
    - * = provides 'canonicalize()' method; 
          see docstring for SmartEnum and individual enums
  
  Class Attributes:
    - expects (InputType): what sort of text input the cipher expects (usually 'InputType.LETTERS')
    - policy (InputPolicy): how to handle input strings that don't match the cipher's expectation (see 'check_type()')
    - output_case (Case): what case to use when reporting the result of the cipher operation
    - should_remove_spaces (bool): whether to remove spaces when reporting the result
    - use_stepwise (bool): when running through a tutorial, call 'self.process(char)' on each char in the input string.
                          This doesn't make sense for all ciphers and is controlled by this bool. 
      
  Instance Attributes:
    - input (str): the input text on which to perform a cipher operation
    - result (str): the result of the cipher operation
    - operation (Operation): which operation to perform; ENCIPHER or DECIPHER
    - suppress_tutorial (bool): prevents the tutorial from running (e.g., Caesar brute force mode)
    - alphabet_list (list): list of individual letters in the alphabet
    - lower_letters/upper_letters (str): case-specific string of alphabet
  
  String representation:
    - "CLASS_NAME"
  
  Implementing Ciphers:
    - Inherit from a 'Cipher' class
      - Use a more specific class (e.g., 'Monoalphabetic', 'KeyedCipher') if applicable
    - Set 'use_stepwise = True' if the tutorial should call 'process()' for each letter in the string
    - Extend '__init__()' with specific initialization logic
    - Override '__str__()' if there is additional information that should be printed
    - Extend 'alphabet_init()' if there are additional procedures to set up the alphabet
    - Implement 'tutor()' if you want users to be able to access a tutorial for the tutor
    - Implement 'process()' to set 'result' to the result of the cipher
  
  See method and child class docstrings for more details
  """
  class Case(Enum):
    """Enumerated case designators: NONE, ORIGINAL, LOWER, UPPER

    See doc for 'canonicalize()' method  
    """
    # This is not a SmartEnum because we actually need dual string matching
    NONE = auto()
    ORIGINAL = auto()
    LOWER = auto()
    UPPER = auto()

    # Canonicalize case designator, with substantially different logic from SmartEnum
    @classmethod
    def canonicalize(cls, input) -> Enum:
      """Canonicalize the input value or string to the corresponding enum member.

      Args:
          input: The value to be canonicalized
              This may be an 'int', 'str', or member of the class. 
              Unlike SmartEnum, some members have two string matches:
                - "capital" or "uppercase" = UPPER
                - "small" or "lowercase" = LOWER
                - "original" = ORIGINAL
                - "none" = NONE

      Returns:
          Enum: The matching enum member

      Raises:
          TypeError: If the input is not of a valid type
          ValueError: If the input is a valid type but no matching enum member is found
      """
      input_type = type(input)
      # If it is already of the correct type, return it back
      if input_type == cls:
        return input
      elif input_type == int:
        for member in cls:
          if member.value == input:
              return member
      elif input_type == str:
        # Recognize partial matches to cases
        # If a full case designator starts with the provided designator...
        if "capital".startswith(input.lower()) or "uppercase".startswith(input.lower()):
          # return the appropriate member
          return cls.UPPER
        elif "small".startswith(input.lower()) or "lowercase".startswith(input.lower()):
          return cls.LOWER
        elif "original".startswith(input.lower()):
          return cls.ORIGINAL
        elif "none".startswith(input.lower()):
          return cls.NONE
        else:
          message = f"Invalid case designator '{input}'"
          message += "\nSpecify all or part of 'capital', 'uppercase', 'small', 'lowercase', 'original', or 'none'"
          raise ValueError(message)
      else:
        raise TypeError(f"Cannot canonicalize {cls} from {input_type}")
      raise ValueError(f"No member in {cls} matching {input}")
  
  class Operation(SmartEnum):
    ENCIPHER = auto(), "en"
    DECIPHER = auto(), "de"
  
  class InputType(Enum):
    UNKNOWN = "unknown"
    DIGITS = "digits"
    LETTERS = "letters"
  
  class InputPolicy(SmartEnum):
    WARN = auto(), "warn"
    SKIP = auto(), "skip"
    CONTINUE = auto(), "continue"

  # Default controls for all ciphers
  expects = InputType.LETTERS
  policy = InputPolicy.WARN
  output_case = Case.NONE
  should_remove_spaces = False
  # Whether to use stepwise function in tutorial
  use_stepwise = False

  def __init__(self, input: str, operation=Operation.ENCIPHER, suppress_tutorial=False) -> None:
    """Initialize self.input, self.operation, and self.suppress_tutorial from params; set self.result to empty str."""
    self.input = input
    self.result = ""
    self.operation = self.Operation.canonicalize(operation)
    self.suppress_tutorial = suppress_tutorial
    self.alphabet_init()
  
  # Generic string method will just return the class name
  def __str__(self) -> str:
    return f"{self.__class__.__name__}"

  # Initialize standard alphabet list
  def alphabet_init(self) -> None:
    """Initialize standard alpha_list, lower_letters, and upper_letters.
    
    Child classes should implement additional logic if needed,
    usually after calling this inherited method.
    """
    # Iterable list of lowercase letters
    self.alpha_list = list(string.ascii_lowercase)
    # Save separate upper and lowercase strings
    self.lower_letters = string.ascii_lowercase
    self.upper_letters = string.ascii_uppercase

  # Basic methods to check if is deciphering or enciphering
  def is_enciphering(self) -> bool:
    return self.operation == self.Operation.ENCIPHER
  
  def is_deciphering(self) -> bool:
    return self.operation == self.Operation.DECIPHER

  # Standardized list formatting for tutorials
  @staticmethod
  def format_list(input: Iterable, width=2, prefix=" ", suffix="", highlight=False) -> str:
    """Format iterable object into string (primarily for printing alphabet list(s) in tutorials)

    Args:
      - input (Iterable): input iterable containing the items to format.
      - width (int, optional): width for each item (left justified). Defaults to 2.
      - prefix (str, optional): prefix before items. Defaults to " " (single space).
      - suffix (str, optional): suffix after items. Defaults to "" (nothing).
      - highlight (bool, optional): whether to call 'Color.highlight()' on the result. Defaults to False.

    Returns:
        str: resulting formatted string
    """
    result = prefix
    for item in input:
      result += str(item).ljust(width)
    result += suffix
    if highlight:
      result = Color.highlight(result)
    return result
  
  # Standardized printing of labeled attributes
  @staticmethod
  def format_labeled(content, label="input", width=10, separator="=") -> str:
    """Format some content with a label (used in tutorials)

    Args:
      - content (Any): the content to format (must have a string representation).
      - label (str, optional): the label for the content. Defaults to "input".
      - width (int, optional): width of the label (right justified). Defaults to 10.
      - separator (str, optional): separator between label and content. Defaults to "=".

    Returns:
      - str: formatted string with label, separator, and content
    """
    return " ".join([label.rjust(width), separator, str(content)])
  
  # Similar to above but speficially for stepwise tutorials showing per-char change
  @staticmethod
  def format_change(old_content: str, new_content: str, old_width=5, separator="->", suffix="") -> str:
    """Variant on 'format_labeled()' tailored for per-char change in stepwise tutorials

    Args:
      - old_content (str): old content to be displayed on the left
      - new_content (str): new content to be displayed on the right
      - old_width (int, optional): width for old_content (right justified). Defaults to 5.
      - separator (str, optional): separator between old and new content. Defaults to "->".
      - suffix (str, optional): content to display at end. Defaults to "" (nothing).

    Returns:
        str: formatted string with old content, separator, new content, and suffix
    
    Note:
      - Both old and new content are surrounded by 'single quotes'
      - If both contents are identical and the separator is the default,
        the separator will be changed to "==".
    """
    # If both contents are the same and separator is default
    if old_content == new_content and separator == "->":
      # Set to double equals to match width of default
      separator = "=="
    # Add quotes to right and new content
    old_content = f"'{old_content}'"
    new_content = f"'{new_content}'"
    old_content = old_content.rjust(old_width)
    return " ".join([old_content, separator, new_content, suffix])

  # Set case for parent cipher class
  @classmethod
  def set_case(cls, new_case) -> None:
    """Set 'cls.output_case' to the canonicalized version of 'new_case' (see Cipher.Case)"""
    cls.output_case = cls.Case.canonicalize(new_case)
  
  # Wait for Enter to continue, used in tutorials
  @staticmethod
  def wait_enter(action="Solve the cipher") -> None:
    """Wait for user input with the message:
    "{action} on your own if you wish, then press Enter to continue: "

    'action' defaults to "Solve the cipher"
    
    If non-interactive, prints message but continues anyway without error.
    """
    # Try to wait for Enter to continue, else just print a newline and continue
    try:
      input(f"{action} on your own if you wish, then press Enter to continue: ")
    except:
      print()
    # Extra newline to get the spacing right
    print()

  # Determine what the input string contains
  def get_input_type(self) -> str:
    """Get 'Cipher.InputType' enum member matching 'self.input'.
    
    Looks at first or second character to ascertain this.
    """
    # Determine content type by first or second letter of input
    if re.search('^.?[0-9]', self.input):
      return self.InputType.DIGITS
    elif re.search('^.?[A-Za-z]', self.input):
      return self.InputType.LETTERS
    else:
      return self.InputType.UNKNOWN
  
  # Print warning if type doesn't match expectation
  def check_type(self) -> bool:
    """Check type of input string

    Returns:
        bool: whether or not the cipher operation should continue

    Description:
      - 'get_input_type()': if it matches 'expects', return True
      - 'policy': if type is wrong, return value and output is determined by this value
      - Otherwise, print a warning message to 'stderr' but still return True
    """
    input_type = self.get_input_type()
    if input_type == self.expects:
      return True
    else:
      # If an assumption has been specified, return from the function with the appropriate return value
      if self.policy == self.InputPolicy.CONTINUE: return True
      elif self.policy == self.InputPolicy.SKIP: return False
      else:
        Logger.error(f"String starts with {input_type.value}, which the {self.__class__.__name__} cipher doesn't expect.")
        Logger.error("The result is unlikely to be useful.")
        Logger.error("(Specify a mismatch policy ('-m') to suppress this message.)")
        return True
  
  # Remove spaces from string
  @staticmethod
  def remove_spaces(input: str) -> str:
    """Remove spaces from the provided input string and return the result."""
    # Implemented by splitting the output into iterable list,
    # then joining it with no char as the separator
    return ''.join(input.split())

  # Prepare result for printing based on specified preferences
  def format_result(self) -> str:
    """Apply customized formatting requested by 'should_remove_spaces' and 'output_case' to the result string

    Returns:
      - str: formatted string
    """
    formatted = self.result
    # If strip spaces requested, perform that change here
    if self.should_remove_spaces: 
      formatted = self.remove_spaces(formatted)
    # Handle requested output case
    if self.output_case == self.Case.UPPER:
      formatted = formatted.upper()
    elif self.output_case == self.Case.LOWER:
      formatted = formatted.lower()
    return formatted

  # Print out report of cipher operation
  def report(self, intro=None, intro_level=Logger.Level.INFO) -> None:
    """Print result of cipher operation

    Args:
      - intro (str, optional): Summary of operation performed. Defaults to string representation of self.
      - intro_level (Logger.Level, optional): Logger level at which to print intro. Defaults to Logger.Level.INFO.
    
    Uses 'format_result()' to apply formatting customizations and
    'Color.highlight()' to highlight the intro portion
    """
    # Use self string representation if no intro parameter provided
    intro = intro or self
    # Automatically highlight intro portion and log at requested level
    Logger.output(intro_level, f"  Processed with {Color.highlight(intro)}:")
    output = self.format_result()
    # Output the processed string
    Logger.normal(output)
  
  # Standard flow for running a cipher operation
  def run(self) -> None:
    """Run full cipher operation

    Steps:
      - 'check_type()': continue based on return value
      - 'tutor()': if 'Logger.get_higher_level()' requests it
      - 'process()': perform cipher operation
      - 'main_report()': wrapper for 'report()' to print resulting string
    """
    # Check for valid type
    if self.check_type():
      # Run tutor if either Logger level requests it.
      # This doesn't appear to have any significant effect on runtime.
      if Logger.get_higher_level() >= Logger.Level.TUTORIAL \
          and not self.suppress_tutorial:
        self.tutor()
      # Process the cipher
      self.process()
      # Almost all the ciphers should report their status after running.
      # Those that don't can wrap the main_report() method with additional checks.
      self.main_report()
  
  # Generic processs method (to be overridden in child classes)
  def process(self) -> None:
    """Perform cipher operation on 'input' and store in 'result'.
    This is empty in the base class and should be overridden in child classes.
    """
    pass  # Does nothing here, just makes it so the generic run method can apply to all child classes
  
  # Generic tutor method
  def tutor(self, input=None, action=None) -> None:
    """Print tutorial information

    Args:
      - input (str, optional): Input string on which to run the tutorial. Defaults to None.
      - action (str, optional): User action to prompt for before continuing (see 'wait_enter()'). Defaults to None.
    
    Operation:
      - If available, print 'self.tutor_string' at tutorial level and with 'dedent()'
      - If an action is specified and print level is high enough, pass action on to 'self.wait_enter()'
      - If no action was specified:
        - Run 'self.wait_enter()' if print level is high enough
        - If 'self.use_stepwise' and 'Logger.get_higher_level()' is high enough:
          - Call 'self.stepwise()' and pass on the provided 'input'

    Usage:
      - Implement 'tutor()' in your child Cipher class
      - Set 'self.tutor_string' to a multiline string containing information about how to perform the cipher operation
      - Call an inherited tutor method, setting the 'action' as needed
      - Repeat for all steps in the tutorial procedure
    """
    # If set, print tutor string
    if hasattr(self, 'tutor_string') and self.tutor_string:
      # Pass string through dedent to remove indentation inherited from 
      # multiline string construct
      Logger.tutorial(dedent(self.tutor_string))
    # The introduction to the tutor will vary by the inidivual cipher,
    # but this logic will apply to several.
    if not action:
      # If no action specified, assume this is the end of the tutorial
      # Check print level directly to determine whether or not to wait for enter.
      if Logger.print_level >= Logger.Level.TUTORIAL:
        # Offer user the opportunity to solve the cipher on their own,
        # then go stepwise if requested.
        self.wait_enter()
      # Check if we should use the stepwise function.
      # self.use_stepwise specifies if it is applicable to the specified cipher and
      # Logger.get_higher_level() indicates whether either set level requests stepwise information.
      # This also doesn't seem to have a material impact on runtime.
      if self.use_stepwise and Logger.get_higher_level() >= Logger.Level.STEPWISE:
        self.stepwise(input)
    else:
      # If an action was provided, it's not the end
      if Logger.print_level >= Logger.Level.TUTORIAL:
        self.wait_enter(action)

  # Generic stepwise method
  def stepwise(self, input=None) -> None:
    """Loop through characters in input and calls 'self.process()' on each. Used in tutorials.

    Args:
      - input (str, optional): input string on which to operate. Defaults to 'self.input'.
    """
    # Input if provided, else self.input
    input = input or self.input
    # Loop through characters in the provided input
    for c in input:
      if c.isalpha():
        # Process alphabetic characters and report result
        self.process(c)
        Logger.stepwise(self.format_change(c, self.result))
      elif c == "\n":
        # Print extra newline to make newlines stand out
        Logger.stepwise("  newline\n")
      else:
        Logger.stepwise(self.format_change(c, c))
    # Extra newline for spacing
    Logger.stepwise("")
  
  # Generic wrapper for report()
  def main_report(self) -> None:
    """Wrapper for 'report()' to be overridden in child classes if the report should not always run."""
    self.report()


class Monoalphabetic(Cipher):
  """Generic class for Monoalphabetic ciphers

  A Monoalphabetic cipher is one that has a consistent replacement for every
  individual letter of the alphabet. 'A' will always be replaced with a particular letter,
  'B' will always be replaced with another letter, etc.

  The 'process()' method will be shared with most Monoalphabetic ciphers.
  It will use 'self.table' as a translation table for the monoalphabetic substitution.

  Extended methods:
    - alphabet_init()
    - process()

  Implementing a monoalphabetic cipher:
    - Inherit from 'Monoalphabetic'
    - Extend '__init__()' with additional initialization logic
    - Extend 'alphabet_init()'
      - Use 'str.maketrans()' to set 'self.table' to the substitution table for the cipher
    - Implement 'tutor()' if you want a tutorial for the cipher
  """
  # These ciphers need the stepwise function for tutorial
  use_stepwise = True
  # Child classes should reimplement this to properly set self.table.
  # It is provided here to avoid errors if this doesn't happen.
  def alphabet_init(self) -> None:
    """Generic alphabet initialization; should be overridden in child classes.
    Sets 'self.table' to a translation table that will make no changes,
    but will prevent errors if this method is not overridden.
    """
    super().alphabet_init()
    # The default table will change nothing
    self.table = str.maketrans(self.lower_letters, self.lower_letters)

  # Child classes shouldn't need to reimplement this class;
  # instead, cipher-specific logic should be in alphabet_init()
  def process(self, input=None) -> None:
    """Translate input string using 'self.table'.

    Args:
      - input (str, optional): Input string to translate. Defaults to 'self.input'.
    """
    # Input if provided, else self.input
    input = input or self.input
    # Use translation table to generate result
    self.result = input.translate(self.table)
    

class KeyedCipher(Cipher):
  """Generic class for ciphers that require a key. Adds logic to set the key.

  Extended methods:
    - __init__()
    - __str__()

  Additional methods:
    - set_key()

  Instance attributes:
    - key (str): the key for the cipher to use

  String representation:
    - "CLASS_NAME KEY"
  """
  def __init__(self, input: str, key: str, operation=Cipher.Operation.ENCIPHER, **kw) -> None:
    self.set_key(key)
    super().__init__(input, operation=operation, **kw)

  # Most keyed ciphers will just print the cipher name and key
  def __str__(self) -> str:
    return f"{self.__class__.__name__} {self.key}"
  
  # Process string as a key
  def set_key(self, new_key: str) -> None:
    """Set key attribute after removing non-alphanumeric characters

    Args:
      - input (str): the key to set

    Raises:
      - ValueError: new key was empty after processing
    """
    # Convert to lowercase
    key = new_key.lower()
    # Use regular expression to match non-alphanumeric characters
    pattern = r'[^a-zA-Z0-9]'
    # Replace non-alphabetic characters with an empty string
    key = re.sub(pattern, '', key)
    # Check for empty key
    if not key:
      raise ValueError(f"{self.__class__.__name__} processed empty key {new_key} (probably no alphanumeric characters)")
    # Set to key attr
    self.key = key


class NumberedCipher(Cipher):
  """Generic class for ciphers that represent text as numbers

  Extended methods:
    - __init__()
    - check_type()
    - process()

  Additional methods:
    - process_alpha(): to be implemented in child classes with specific logic for the cipher
    - process_index(): to be implemented in child classes with specific logic for the cipher
  
  Class attributes:
    - separator (str): content to use between numbers in the ciphertext
  """
  separator = "-"
  def __init__(self, input: str, **kw) -> None:
    super().__init__(input, **kw)
  
  def check_type(self) -> bool:
    """Always return True because numbered ciphers interpret both types of input."""
    return True

  def process_alpha(self, old_char: str) -> str:
    return old_char

  def process_index(self, index: int) -> int:
    return index

  # Main processing functionality
  def process(self, input=None) -> None:
    """Process input by iterating through each character."""
    input = input or self.input
    new_index = 0
    # Iterate over characters in input
    for char in input:
      # Original and new content added this round
      old_char = new_char = ''
      # Python added case-like match structures in 3.10
      # Using if-elif instead for better compatibility
      if char.isalpha():
        old_char += char
        # If the current last character in the resulting string is a digit,
        # we will need to add a separator to distinguish the different indices 
        if re.search('[0-9]$', self.result): 
          # Also need to check that the string doesn't end with a newline
          # Not necessary in bash due to different handling of the $ anchor
          if not re.search('\n$', self.result):
            new_char += self.separator
        new_char += self.process_alpha(char)
      elif char.isdigit():
        # Add the digit character to the index variable.
        # Because Python actually cares about types, 
        # we're concatenating as strings then casting back to int
        new_index = int( str(new_index) + char )
      else:
        # Any other character indicates the end of a numbered index.
        if new_index != 0:
          old_char += str(new_index)
        new_char += self.process_index(new_index)
        # Clear the index variable for next time
        new_index = 0
        # If the special character is anything other than a newline,
        # we should report it as part of the original content
        if char != '\n':
          old_char += char
          # If the special character is also not the chosen separator, should be passed on to result
          if char != self.separator: new_char += char
      # If new content is not empty, report change
      if new_char != '':
        Logger.stepwise(self.format_change(old_char, new_char, old_width=6))
      # Append new content to resulting string
      self.result += new_char
      # Account for newline characters separately for better step-by-step output
      if char == '\n':
        self.result += char
        Logger.stepwise("    newline\n")
    
    # Check for extra append
    if new_index != 0:
      new_char += self.process_index(new_index)
      # One more append in case the last character is a digit
      self.result += new_char
      Logger.stepwise(self.format_change(old_char, new_char))
      # Extra newline
      Logger.stepwise()


class MixedAlphabet(KeyedCipher):
  """Generic class for ciphers that use a mixed alphabet 
  (letters in key moved to start of list).

  Extented methods:
    - alphabet_init(): sets 'self.table' to use the mixed alphabet directly and
    - tutor(): contains instructions on creating the mixed alphabet.
  """
  def alphabet_init(self) -> None:
    """Rearrange 'self.alpha_list' to be a mixed alphabet and
    set 'self.table' to substitute using that list of letters (depending on operation).
    """
    super().alphabet_init()
    # Index showing start of normal alphabet
    alphabet_start = 0
    # Loop through characters in key
    for char in self.key:
      # Verify that k is a letter (if not, nothing to do)
      if char.isalpha():
        # Get the index of the key character
        key_index = self.alpha_list.index(char.lower())
        # If the key character is at or past the alphabetic start index,
        # we are at its first occurrence in the key
        if key_index >= alphabet_start:
          # Loop from kin down to asi (if equal, will do nothing)
          for i in range(key_index, alphabet_start, -1):
            # Move the characters right one space in this range
            self.alpha_list[i] = self.alpha_list[i - 1]
          # Set the alphabet at asi to key character
          self.alpha_list[alphabet_start] = char
          # Increment alphabet start
          alphabet_start += 1
    
    # Construct translation strings
    self.lower_translation = ''.join(self.alpha_list)
    self.upper_translation = self.lower_translation.upper()
    # And construct translation table
    if self.is_deciphering():
      # Order depends on the direction
      self.table = str.maketrans(self.lower_translation + self.upper_translation,
                                self.lower_letters + self.upper_letters)
    else:
      self.table = str.maketrans(self.lower_letters + self.upper_letters,
                                self.lower_translation + self.upper_translation)
    
  def tutor(self, input=None) -> None:
    """Add and print tutorial information pertaining to forming the mixed alphabet.
    Child classes can put their own information in 'self.tutor_string', then call this method
    to add information about constructing the mixed alphabet.

    Args:
      - input (str, optional): what to provide tutorial on. Defaults to None (later subsituted for 'self.input').
    """
    # This will add to the string that was started by a child class
    self.tutor_string += f"""
      To construct the mixed alphabet:
        1. Write out all {Color.highlight("unique")} letters in the provided key: {Color.highlight(self.key)}
          a. Only the {Color.highlight("first occurrence")} of duplicate letters should remain
        2. After the key, write out all remaining letters of the alphabet in their regular order

      Alternately, you may prefer to think of it in the way that the script accomplishes it:
        1. Start with the normal alphabet
        2. For each {Color.highlight("unique")} letter in the provided key:
          a. Move it to just before the first letter in the {Color.highlight("untouched")} portion of the alphabet

      {self.format_labeled(self.key, "key")}
      """
    super().tutor(input, "Construct the alphabet")


class PolybiusSquare(MixedAlphabet):
  """Generic class for ciphers that use a Polybius square.

  Requires that 'j' be treated as 'i' in all situations and 
  depicts the mixed alphabet in a 5x5 square.

  Extended methods:
    - __init__(): substitute 'j' for 'i' in input string
    - alphabet_init(): substitutes 'j' for 'i' in key and alphabet list
    - tutor(): inherits tutorial information from 'MixedAlphabet' and adds Polyius square instructions

  Additional methods:
    - get_square(): print 5x5 representation of Polybius square
    - get_square_item(): get item from provided row, column coordinates
    - get_square_indexes(): get row, column location of provided item
  """
  def __init__(self, input: str, key: str, operation=Cipher.Operation.ENCIPHER, **kw) -> None:
    super().__init__(input, key, operation, **kw)
    # Replace 'j' in input string
    self.input = input.translate(self.no_j)

  def alphabet_init(self) -> None:
    """Extend inherited logic by replacing 'j' for 'i' in key and alphabet list"""
    # Need to remove j from key before proceeding with intialization of alphabet
    self.no_j = str.maketrans("j", "i")
    self.key = self.key.translate(self.no_j)
    # Call inherited initialization logic
    super().alphabet_init()
    # Remove 'j' from alphabet list
    self.alpha_list.remove("j")
  
  def tutor(self) -> None:
    # Start by running mixed alphabet tutorial
    MixedAlphabet.tutor(self)
    # Construct polybius square tutorial
    self.tutor_string = f"""
    To construct the Polybius square in a 5x5 grid:
      1. Remove the letter {Color.highlight("'j'")} from the alphabet
        a. We only have 25 total spaces in the square, so we need to drop 1 letter
        b. Instances of {Color.highlight("'j'")} will be replaced with {Color.highlight("'i'")}
      2. Place the {Color.highlight("first 5")} characters in the alphabet on the first row
      3. Place the {Color.highlight("next 5")} characters in the second row and {Color.highlight("repeat")} for the other 3 rows
    
    {self.format_list(self.alpha_list)}
    """
    Cipher.tutor(self, action="Construct the square")
  
  # Special Polybius square methods
  # Get string representation of Polybius square
  def get_square(self, prefix="") -> str:
    # Construct square as string
    square = f"""
        1 2 3 4 5
      1 {' '.join(self.alpha_list[0:5])}
      2 {' '.join(self.alpha_list[5:10])}
      3 {' '.join(self.alpha_list[10:15])}
      4 {' '.join(self.alpha_list[15:20])}
      5 {' '.join(self.alpha_list[20:25])}
      """
    # Remove indentation from multiline string
    square = dedent(square)
    # Re-add indentation with the specified prefix
    return indent(square, prefix)
  
  # Get item from row and column indexes
  def get_square_item(self, row: int, column: int) -> str:
    # Account for rollover and reset to 0 start
    row = (int(row) - 1) % 5
    column = (int(column) - 1) % 5
    # Get corresponding index in regular array and get value from that.
    # Observe the layout of get_square() if this doesn't make sense.
    regular_index = row * 5 + column
    return self.alpha_list[int(regular_index)]

  # Get row and column indexes from item
  def get_square_indexes(self, item: str) -> tuple:
    # Find index of requested item
    regular_index = self.alpha_list.index(item.lower())
    # Get row and column location.
    # Observe the layout of get_square() if this doesn't make sense.
    # In Python, // is integer division
    row = (regular_index // 5) + 1
    column = (regular_index % 5) + 1
    # Return indexes as tuple
    return row, column


### Supported ciphers


class Caesar(Monoalphabetic):
  """Caesar Cipher class

  The Caesar cipher shifts all letters left or right X spaces in the alphabet (default 3 right)
  
  Enums:
    - Direction: R, L (SmartEnum)
  
  Extended methods:
    - __init__()
    - __str__()
    - alphabet_init()
    - tutor()

  Instance Attributes:
    - dist (int): the distance of the shift; how many positions in the alphabet to replace each letter with
    - direction (Direction): the direction (right or left) of the shift
  
  String representation:
    - "CLASS_NAME DIRECTION DISTANCE"
  """
  # Enumerate directions
  class Direction(SmartEnum):
    RIGHT = auto(), 'R'
    LEFT = auto(), 'L'

  def __init__(self, input: str, dist=3, direction=Direction.RIGHT, suppress_tutorial=False, **kw) -> None:
    """Set dist and direction from params, pass others on to inherited initializers"""
    # Set additional Caesar attributes
    # Use modulo to account for wraparound
    self.dist = dist % 26
    self.direction = self.Direction.canonicalize(direction)
    # Run inherited initializations
    super().__init__(input, **kw)

  def __str__(self) -> str:
    return f"Caesar {self.direction} {self.dist}"
  
  # Initialize translation table
  def alphabet_init(self) -> None:
    """Initialize translation table for the specified distance and direction"""
    super().alphabet_init()
    # Shift lower and upper case letters by dist value
    lower_translation = self.lower_letters[self.dist:] + self.lower_letters[:self.dist]
    upper_translation = self.upper_letters[self.dist:] + self.upper_letters[:self.dist]

    # Create translation table by combining lowercase and uppercase translations
    if self.direction == self.Direction.LEFT:
      # Order depends on the direction
      self.table = str.maketrans(lower_translation + upper_translation,
                                self.lower_letters + self.upper_letters)
    else:
      self.table = str.maketrans(self.lower_letters + self.upper_letters,
                                lower_translation + upper_translation)
  
  # Provide tutorial for Caesar cipher
  def tutor(self, input=None) -> None:
    # If this run is a brute force run, skip the tutorial
    if self.suppress_tutorial: return
    # Input if provided, else self.input
    input = input or self.input

    self.tutor_string = f"""
     == Caesar Cipher tutorial ==

    The Ceasar Cipher replaces all letters in the string 
    with the letter DIST positions in the alphabet away from the original letter.
    The default Caesar distance is 3.
    Typically a RIGHT shift is used to encipher text and LEFT to decipher.

    To perform the Caesar cipher with the current settings,
     1. Locate each character in the input string
     2. Replace it with the character that is {self.dist} spaces to the {self.direction}
        a. Pass through spaces, punctuation, other special characters

    {self.format_labeled(self.input)}
    {self.format_labeled(f"{self.dist} {self.direction}", label="operation")}

    {self.format_list(self.alpha_list)}
    """
    # Finish with inherited tutor logic
    super().tutor(input)


class Atbash(Monoalphabetic):
  """Atbash Cipher class

  The Atbash cipher replaces each letter with the letter in its opposite position in the alpahbet
  
  Extended methods:
    - alphabet_init()
    - tutor()
  """
  def alphabet_init(self) -> None:
    """Initialize translation table with a reversed alphabet"""
    super().alphabet_init()
    # Reverse lower and upper case letters (these will be useful outside of this method)
    # Using the syntax [START:END:STEP] (so using the whole string but traversed backwards)
    self.lower_translation = self.lower_letters[::-1]
    self.upper_translation = self.upper_letters[::-1]
    # Create translation table
    self.table = str.maketrans(self.lower_letters + self.upper_letters,
                              self.lower_translation + self.upper_translation)
  
  def tutor(self, input=None) -> None:
    # Input if provided, else self.input
    input = input or self.input

    self.tutor_string = f"""
     == Atbash Cipher tutorial ==

    The Atbash Cipher translates all letters in the string to their complement in the alphabet. (e.g., A -> Z, B -> Y)
    It is a reciprocal cipher; the same steps are used to encipher and decipher text.

    To perform the Atbash cipher,
     1. Locate each character in the normal alphabet
     2. Replace it with the character in the same position in the reversed alphabet
        a. Pass through spaces, punctuation, other special characters
    
    {self.format_labeled(self.input)}

    {self.format_list(self.alpha_list)}
    {self.format_list(reversed(self.alpha_list))}
    """
    super().tutor(input)


class A1Z26(NumberedCipher):
  """A1Z26 Cipher class

  The A1Z26 Cipher substitutes letters for their numeric position in the alphabet
  
  Extended methods:
    - alphabet_init()
    - tutor()
    - process_alpha()
    - process_index()
  """
  def alphabet_init(self) -> None:
    """Modifies the standard alphabet list by adding an empty element at 0 for two reasons:
      1. This puts 'a' at index 1, so we can directly use A1Z26 numbers as the array index
      2. This allows appending alpha[0] to the resulting string ad nauseam with no consequences
    """
    super().alphabet_init()
    self.alpha_list.insert(0, "")

  # General tutor information
  def tutor(self) -> None:
    self.tutor_string = f"""
     == A1Z26 Cipher tutorial ==

    The A1Z26 Cipher represents letters by their numeric position in the alphabet.
    This is summarized in the name; A=1, Z=26.

    To perform the A1Z26 cipher,
     1. Locate each character or number in the input string
     2. Replace it with the correlating item of the other type
        a. If you are enciphering into numbers, add the separator ('{self.separator}') between successive numbers
        b. If you are deciphering from numbers, remove the separators between the numbers
        c. Pass through spaces, punctuation, other special characters
    
    {self.format_labeled(self.input)}

    {self.format_list(range(1,27), width=3)}
    {self.format_list(self.lower_letters, width=3)}
    """
    super().tutor()

  def process_alpha(self, old_char: str) -> str:
    # Return the index of the input char
    return str(self.alpha_list.index(old_char.lower()))

  def process_index(self, index: int) -> str:
    # Add to the resulting string the alphabet character at the current index.
    # This sometimes adds the first item in the array to the string,
    # which doesn't matter since the first item is ''
    return self.alpha_list[index]


class Vigenere(Caesar, KeyedCipher):
  """Vigenere Cipher class

  The Vigenere cipher applies an offset to individual letters based on the provided key
  
  Extended methods:
    - __init__()
    - __str__()
    - tutor()
    - process()

  Class Attributes:
    - use_stepwise (bool): inherited from Cipher, changed to 'False' for Vigenere

  Instance Attributes:
    - use_autokey (bool): whether to use the Autokey method when processing the input text
  
  String representation:
    - "CLASS_NAME DIRECTION KEY"
  """
  # Caesar uses stepwise but Vigenere doesn't; correct that here
  use_stepwise = False
  # Whether to use autokey (can also be overridden at instantiation)
  use_autokey = False
  def __init__(self, input: str, key: str, operation=Cipher.Operation.ENCIPHER, direction=Caesar.Direction.RIGHT, use_autokey=None) -> None:
    # Call inherited initializers
    super().__init__(input, key=key, direction=direction, operation=operation)
    # Additional Vigenere-specific stuff
    # Only set custom use_autokey if it's a bool
    if isinstance(use_autokey, bool):
      self.use_autokey = use_autokey
    # If using autokey and enciphering, add input to key
    if self.use_autokey and self.is_enciphering():
      self.set_key(key + input)
  
  def __str__(self) -> str:
    return f"{self.__class__.__name__} {self.direction} {self.key}"
  
  def tutor(self) -> None:
    # The formatting of these multiline strings is carefully set so that
    # the Cipher.tutor() method can dedent it and get the right relative indentation of the lines.
    self.tutor_string = """
      == Vigenere Cipher tutorial ==

      The Vigenere Cipher is a more elaborate Caesar cipher in which the offset varies per letter.
      The alphabetical position of the letters in the key provided to the cipher determines this per-letter offset.
      """
    # Need extra tutor information if using autokey variant
    if self.use_autokey:
      if self.is_enciphering():
        autokey_instruction = "the input text can be immediately appended to the key."
      else:
        autokey_instruction = "the plain text must be appended to the key as it is deciphered."
      self.tutor_string += f"""
      The Autokey variant has been selected, so the plain text should be appended to the key.
      It is currently set to {Color.highlight(self.operation)}, so {autokey_instruction}
      """
    self.tutor_string += f"""
      To perform the Vigenere cipher,
       1. Line up each alphabetic input character with the corresponding alpha or numeric character in the key
          a. Pass through all non-alpha input characters
          b. Repeat the key as needed
       2. Locate the key character's index in the alphabet (or literal value if a digit)
       3. Replace the input character with the character that many characters to the {self.direction}
      """
    # Additional autokey special sauce
    if self.use_autokey and self.is_deciphering():
      self.tutor_string += "    a. (Autokey) Add the new character to the end of the key\n"
    self.tutor_string += f"""
      {self.format_labeled(self.input)}
      {self.format_labeled(self.key, label="key")}
      {self.format_labeled(f"{self.operation} {self.direction}", label="operation")}

      {self.format_list(range(0,26), width=3)}
      {self.format_list(self.lower_letters, width=3)}
      """
    # Explicitly call the inherited Cipher tutor method 
    # (so it doesn't go to the inherited Caesar method)
    Cipher.tutor(self)

  def process(self) -> None:
    # Create empty local result string (self.result will be used by Caesar component of self)
    temp_result = ""
    # Index tracking where we are in the key
    key_index = 0
    # Initialize Caesar object for processing individual characters
    #local_caesar = Caesar(self.input, direction=self.direction)

    # Iterate over characters in input
    for char in self.input:
      # This is recalculated each loop run for compatibility with deciphering autokey
      key_length = len(self.key)
      # Key characters should only match up with alphabetic input characters
      if char.isalpha():
        # Find the corresponding offset character using modulo so it wraps around 
        # when istr is longer than vkey, which is a common occurrence (unless autokey)
        key_char = self.key[key_index % key_length]
        # Assign the offset to the index of the key char
        # Since we're using the builtin index function, 
        # we need to manually account for digits in key (Gronsfeld variant)
        if key_char.isdigit(): self.dist = int(key_char)
        else: self.dist = self.alpha_list.index(key_char.lower())
        # Re-initialize tables
        super().alphabet_init()
        # Process char as Caesar
        Caesar.process(self, char)
        # Add Caesar result to local result
        temp_result += self.result
        # If deciphering with the autokey method, append deciphered alphanumeric characters to the key
        if self.use_autokey and self.is_deciphering(): 
          self.key += self.result
        # Increment key index
        key_index += 1

        # Step-by-step commentary
        Logger.stepwise(f"{self.format_change(char, self.result)} ( {self.format_labeled(str(self.dist), key_char, width=1)} )")
      else:
        # Key characters should only match up with alpha characters, 
        # so key index is only incremented when encountering alpha in the input (previous branch).
        # Here, we just append whatever other char we have to the result
        temp_result += char

        if char == '\n':
          Logger.stepwise("  newline\n")
        else:
          Logger.stepwise(self.format_change(char, char))
      
    # Extra stepwise newline after loop
    Logger.stepwise()
    # Store result
    self.result = temp_result


class Autokey(Vigenere):
  """Vigenere Autokey class (same as Vigenere but with 'use_autokey=True' for convenience)"""
  use_autokey = True


class Beaufort(KeyedCipher):
  """Beaufort Cipher class

  The Beaufort cipher is a symmetric substitution cipher similar to the Vigenere cipher
  
  Extended methods:
    - tutor()
    - process() 
  """
  def tutor(self) -> None:
    self.tutor_string = f"""
      == Beaufort Cipher tutorial ==

      The Beaufort cipher is similar to the Vigenere cipher in that it processes
      each input character differently based on the provided key.
      It is different in that it is a *reciprocal* cipher,
      so the same procedure is used to encipher and decipher text.

      To perform the Beaufort cipher:
       1. Line up each alphabetic input character with the corresponding alpha or numeric character in the key
          a. Pass through all non-alpha input characters
          b. Repeat the key as needed
       2. Find the alphabetic indexes for both the input and key characters
       3. The resulting character is the character *input* spaces to the L from the *key* character
          a. To summarize, result = key - input
          b. Refer to the positive or negative indices listed below depending on the result

      {self.format_labeled(self.input)}
      {self.format_labeled(self.key, "key")}
      {self.format_labeled("result = key - input", "formula")}

      {self.format_list(range(0,26), width=3, suffix=" - positive indices")}
      {self.format_list(self.alpha_list, width=3, highlight=True)}
      {self.format_list(range(26,0,-1), width=3, suffix=" - negative indices")}
      """
    super().tutor()
  
  def process(self) -> None:
    # Will modify result directly in this function
    self.result = ""
    # Initialize local trackers
    key_index = 0
    key_length = len(self.key)
    # Iterate over characters in istr
    for char in self.input:
      # Check if the character is alphabetic
      if char.isalpha():
        # Find the corresponding offset character using modulo so it wraps around 
        # when istr is longer than vkey, which is a common occurrence (unless autokey)
        key_char = self.key[key_index % key_length]
        # Find alphabetic indices for key and input character
        key_char_index = self.alpha_list.index(key_char.lower())
        in_char_index = self.alpha_list.index(char.lower())
        # The index of the result character is 
        # the index of the key character minus the index of the input character
        new_char_index = key_char_index - in_char_index
        # Add the result character to the result string
        new_char = self.alpha_list[new_char_index]
        self.result += new_char
        # Increment key index
        key_index += 1

        # Step-by-step commentary
        step_suffix = f" -- ( {key_char}={key_char_index} ) - ( {char}={in_char_index} ) = ( {new_char_index}={new_char} )"
        Logger.stepwise(self.format_change(char, new_char, suffix=step_suffix))
      else:
        # Key characters should only match up with alpha characters, 
        # so kin is only incremented when encountering alpha in the input (previous branch)
        # Here, we just append whatever other char we have to wstr
        self.result += char

        if char == '\n':
          Logger.stepwise("  newline\n")
        else:
          Logger.stepwise(self.format_change(char, char))
    
    Logger.stepwise()


class Keyword(MixedAlphabet, Monoalphabetic):
  """Keyword Cipher class

  The Keyword cipher uses a keyword to generate a mixed alphabet as the substitution alphabet.

  Processing for this cipher is handled by inheritance from MixedAlphabet and Monoalphabetic.
  
  Extended methods:
    - tutor()
  """
  # No custom initialization for this class
  def tutor(self, input=None) -> None:
    self.tutor_string = f"""
      == Keyword Cipher tutorial ==

      The Keyword cipher is a monalphabetic substitution cipher that
      uses the provided key to construct a mixed cipher alphabet.
      """
    # Run the mixed alphabet tutorial
    # (this will add to and print the tutor string declared above)
    MixedAlphabet.tutor(self, input)
    # Next leg of tutorial
    prefix_string = "Now that we have the mixed alphabet constructed:"
    if self.is_deciphering():
      self.tutor_string = f"""
      {prefix_string}
        1. Locate each input character in the {Color.highlight("mixed")} alphabet
        2. Replace it with the character in the same position in the {Color.highlight("normal")} alphabet
      """
    else:
      self.tutor_string = f"""
      {prefix_string}
        1. Locate each input character in the {Color.highlight("normal")} alphabet
        2. Replace it with the character in the same position in the {Color.highlight("mixed")} alphabet
      """
    self.tutor_string += f"""
    {self.format_labeled(self.input)}

    {self.format_list(self.lower_letters)}
    {self.format_list(self.lower_translation)}
    """
    # Run normal cipher tutorial routine
    Cipher.tutor(self, input)


class Polybius(PolybiusSquare, NumberedCipher):
  """Polybius Cipher class

  The Polybius cipher directly represents input letters by their position on the Polybius square
  
  Extended methods:
    - __init__()
    - process_alpha()
    - process_index()
    - tutor()
  """
  def __init__(self, input: str, key: str, operation=Cipher.Operation.ENCIPHER, **kw) -> None:
    super().__init__(input, key=key, operation=operation, **kw)

  def process_alpha(self, old_char: str) -> str:
    row, column = self.get_square_indexes(old_char)
    return str(row) + str(column)

  def process_index(self, index: int) -> str:
    try:
      row, column = list(str(index))
      result = self.get_square_item(row, column)
    except Exception:
      # If we can't get the row and column, just return nothing
      result = ""
    return result
  
  def tutor(self) -> None:
    self.tutor_string = """
       == Polybius Cipher tutorial ==
    
    The Polybius cipher is the most basic cipher that uses the Polybius square.
    Like the A1Z26 cipher, it encodes letters as numbers representing their position.
    Unlike A1Z26, which uses a regular alphabetic order, Polybius uses two digits for each letter,
    representing the row and column position on a polybius square.
    """
    PolybiusSquare.tutor(self)
    # Final instructions
    self.tutor_string = f"""
    Now that we have the Polybius square, we can perform the cipher:
      1. Replace all j's in the input string with i
      2. Locate each input character on the Polybius square
        a. Enciphering: replace each letter with the row number, then column number
          i. Add the separator character ({self.separator}) between adjacent number pairs
        a. Deciphering: interpret each number pair as row number, then column number
        b. Pass through spaces, punctuation, other special characters
    
    {self.get_square("      ")}
    """
    Cipher.tutor(self)


class Playfair(PolybiusSquare):
  """Playfair Cipher class

  The Playfair cipher is a digraphic cipher that uses the Polybius square.
  
  Extended methods:
    - tutor()
    - process()
  """
  def tutor(self) -> None:
    # Set initial cipher-specific information
    self.tutor_string = """
       == Playfair Cipher tutorial ==

      The Playfair cipher is a digram substitution cipher that uses
      a mixed alphabet and Polybius square to process two characters at once.
      """
    # Call Polybius (and by extension MixedAlphabet) tutorial
    PolybiusSquare.tutor(self)
    # Next leg of tutorial
    self.tutor_string = f"""
      Now that we have the Polybius square, we can perform the Playfair cipher:
        1. Replace all j's in the input string with i
        2. Identify each pair of letters in the input string
          a. If you encounter a pair of the {Color.highlight("same letter")}, add {Color.highlight("'x'")} in between
            ({self.format_change("falls", "fa lx ls")})
          b. If you reach the {Color.highlight("end")} of the string with just one letter, add an extra {Color.highlight("'x'")}
            ({self.format_change("welcome", "we lc om ex")})
        3. Locate the pair of letters in the Polybius square and determine which rule it matches:
      """
    # Common examples in plaintext and ciphertext
    # For matching columns
    column_pt = f"{self.get_square_item(1,2)}{self.get_square_item(2,2)}"
    column_ct = f"{self.get_square_item(2,2)}{self.get_square_item(3,2)}"
    # For matching rows
    row_pt = f"{self.get_square_item(2,3)}{self.get_square_item(2,4)}"
    row_ct = f"{self.get_square_item(2,4)}{self.get_square_item(2,0)}"
    # For matching neither (this step is reciprocal)
    neither_pt = f"{self.get_square_item(2,4)}{self.get_square_item(1,0)}"
    neither_ct = f"{self.get_square_item(2,0)}{self.get_square_item(1,4)}"
    # Customize instructions based on operation
    if self.is_deciphering():
      self.tutor_string += f"""
          a. If they are on the {Color.highlight("same column")}, replace each letter with the one immediately {Color.highlight("above")} it
              ({self.format_change(column_ct, column_pt)})
          b. If they are on the {Color.highlight("same row")}, replace each letter with the one immediately to the {Color.highlight("left")}
              ({self.format_change(row_ct, row_pt)})"""
    else:
      self.tutor_string += f"""
          a. If they are on the {Color.highlight("same column")}, replace each letter with the one immediately {Color.highlight("below")} it
              ({self.format_change(column_pt, column_ct)})
          b. If they are on the {Color.highlight("same row")}, replace each letter with the one immediately to the {Color.highlight("right")}
              ({self.format_change(row_pt, row_ct)})"""
    # Matching neither is reciprocal; same whether enciphering or deciphering
    self.tutor_string += f"""
          c. If {Color.highlight("neither")} of those match, imagine a rectangle around the letter pair and select the other corners of it.
             Each letter should be replaced with the other corner that is in the {Color.highlight("same row")}
              ({self.format_change(neither_pt, neither_ct)})
        4. (opt.) The script doesn't do this because it's difficult to do programatically,
           but you may wish to add spaces and special characters back in where they make sense
      
      {self.format_labeled(self.input)}

      {self.get_square("      ")}
      """
    # Call standard tutor method
    Cipher.tutor(self)
  
  def process(self) -> None:
    self.result = ''
    # Loop through individual characters in the input string.
    # Using while loop because the step size is irregular based on factors in the loop
    # and Python doesn't allow modifying the iterator in the loop (for good reason)
    i = 0
    while i < len(self.input):
      # Get current and next characters from input
      try:
        char1 = self.input[i]
      except:
        # If can't get cur, just increment i and go to next loop run,
        # which will probably exit the loop due to being out of range.
        i += 1
        continue
      try:
        char2 = self.input[i + 1]
      except:
        # If can't get the next char, probably end of string
        # For end of string, it should be blank (to be replaced with 'x')
        char2 = ''
      
      if char1.isalpha():
        # If the next value is empty, it's the end of the string
        if char2 == '':
          # Pad with 'x'
          char2 = 'x'
          # We are processing one char this loop run
          i += 1
        # If the two characters are the same
        elif char2 == char1:
          # Pad with 'x'
          char2 = 'x'
          # We are processing one char this loop run
          i += 1
        # If the next value is another alpha character, we will skip it next loop
        elif char2.isalpha():
          # We are processing two chars this loop run
          i += 2
        else:
          # If not a letter, we need to delete it from the string
          # Reassign wstr to two slices of itself excluding the offending char
          self.input = self.input[:i + 1] + self.input[i + 2:]
          # We are processing no chars this loop run
          # No change to i and we should continue right on to the next loop iteration
          continue
          
        # Get row/column values
        row1, column1 = self.get_square_indexes(char1)
        row2, column2 = self.get_square_indexes(char2)

        # New content added this loop run
        new_content = ''

        # If they're in the same "column"
        if column1 == column2:
          # Each character is "up" one row for deciphering (down for enciphering)
          if self.is_deciphering():
            new_content = self.get_square_item(row1 - 1, column1) + self.get_square_item(row2 - 1, column2)
          else:
            new_content = self.get_square_item(row1 + 1, column1) + self.get_square_item(row2 + 1, column2)
        # If they're in the same "row"
        elif row1 == row2:
          # Each character is to the left one column for deciphering (right for enciphering)
          if self.is_deciphering():
            new_content = self.get_square_item(row1, column1 - 1) + self.get_square_item(row2, column2 - 1)
          else:
            new_content = self.get_square_item(row1, column1 + 1) + self.get_square_item(row2, column2 + 1)
        # If neither x nor y match between them
        else:
          # Swap x-values, same for (en/de)ciphering
          new_content = self.get_square_item(row1, column2) + self.get_square_item(row2, column1)
        
        # Append with space at end to show digrams
        self.result += new_content + ' '

        # Step-by-step commentary if verbose enough
        Logger.stepwise(self.format_change(char1 + char2, new_content))
      else:
        # If cur is not a letter, we need to delete it from the string
        # Reassign wstr to two slices of itself excluding the offending char
        self.input = self.input[:i] + self.input[i + 1:]
        # We are processing no chars this loop run
        # No change to i and we should continue right on to the next loop iteration
        continue


class Bifid(PolybiusSquare):
  """Bifid Cipher class

  The Bifid cipehr is a digraphic cipher that uses the Polybius square, similar to Playfair.
  
  Extended methods:
    - __init__()
    - __str__()
    - tutor()
    - run()
    - process()
  
  Additional methods:
    - make_lists()

  Instance attributes:
    - period (int): frequency with which to intersperse row and column values
  
  String representation:
    - "{INHERITED_STR} PERIOD"
  """
  # Need to set period in initializer
  def __init__(self, input: str, key: str, period=5, operation=Cipher.Operation.ENCIPHER) -> None:
    self.period = period
    super().__init__(input, key, operation)

  def __str__(self) -> str:
    return super().__str__() + f" {self.period}"

  def make_lists(self) -> None:
    """Make lists of row and column values for the result.
    This contains most of the special sauce for the cipher run,
    implemented as a separate function because it needs to run before the tutorial.
    """
    # Initialize lists
    self.input_indexes, self.input_rows, self.input_columns = [], [], []
    # Loop through individual characters
    for char in self.input:
      # If it's an alphabetic character, get the index
      if char.isalpha():
        row, column = self.get_square_indexes(char)
        # Assign to arrays depending on operation
        if self.is_deciphering():
          self.input_indexes.append(row)
          self.input_indexes.append(column)
        else:
          self.input_rows.append(row)
          self.input_columns.append(column)
    # Now the initial indexes are harvested from the input string.
    # The rest of making the lists is dependent on the operation.
    # Initialize output lists.
    # These are separate from the input lists so that a logic error
    # will result in an empty result instead of an incorrect result.
    self.output_indexes, self.output_rows, self.output_columns = [], [], []
    if self.is_enciphering():
      # The logic for enciphering is quite simple.
      # Loop through values from 0 to the number of row values in period increments
      for i in range(0, len(self.input_rows), self.period):
        self.output_indexes += self.input_rows[i : i + self.period]
        self.output_indexes += self.input_columns[i : i + self.period]
      # To simplify the logic in process(), separate into column and row lists.
      # Accomplished by slicing every other item starting with 0 (even elements) or 1 (odd elements).
      self.output_rows = self.output_indexes[0::2]
      self.output_columns = self.output_indexes[1::2]
    else:
      # The logic for deciphering is more complicated.
      # Copy of the period value for local use and overriding if needed
      # to account for string lengths that aren't a multiple of period.
      interval = self.period
      # Check and report potential problem if there is an odd number of indexes
      if len(self.input_indexes) % 2:
        Logger.error("Bifid(): input_indexes has an odd number of items; result may be incorrect")
      # Loop through every other multiple of period
      for i in range(0, len(self.input_indexes), self.period * 2):
        # Each run process the next period * 2 items.
        # We need to check if this will place it past the end of the array.
        # diff is the number of items it will try to process this run
        # that do NOT have array elements corresponding to them
        diff = i + (self.period * 2) - len(self.input_indexes)
        # If it is negative or zero, we're good.
        if diff > 0:
          # If it is positive, we need to alter the behavior so that 
          # half of the remaining elements are added as row and column values each.
          # For this run, the interval on which it operates needs to be
          # diff / 2 less than the typical period (with integer division).
          interval = self.period - (diff // 2)
        # Add the next interval items as row values
        self.output_rows += self.input_indexes[i : i + interval]
        # Add the next interval items after that as columns
        self.output_columns += self.input_indexes[i + interval : i + (2 * interval)]

  def tutor(self) -> None:
    # Set initial cipher-specific information
    self.tutor_string = f"""
       == Bifid Cipher tutorial ==

      The Bifid cipher is another somewhat complicated cipher
      that uses a mixed alphabet and Polybius square.
      The cipher then intersperses {Color.highlight("row")} and {Color.highlight("column")} values to mask the contents.
      """
    # Call Polybius (and by extension MixedAlphabet) tutorial
    PolybiusSquare.tutor(self)
    # Construct the lists
    self.tutor_string = f"""
      Now that we have the Polybius square, we can start with the cipher by collecting row/column values:
        1. For each letter in the input string, locate it in the Polybius square """
    if self.is_deciphering():
      self.tutor_string += f"""
        2. Add the row and column values to a {Color.highlight("single list")} of coordinates
      """
    else:
      self.tutor_string += f"""
        2. Add the row and column values to {Color.highlight("separate lists")}
      """
    self.tutor_string += f"""
      {self.format_labeled(self.input)}

      {self.get_square("      ")}
      """
    # Prompt for and process the lists, depending on operation
    if self.is_deciphering():
      Cipher.tutor(self, action="Create the list")
      self.tutor_string = f"""
      With the full list of coordinates, we will need to split them up into row and column values:
        1. Add the {Color.highlight(f"first {self.period}")} (current period value) numbers to the list of row values
        2. Add the {Color.highlight(f"next {self.period}")} numbers to the list of column values
        3. Repeat until you have less than {Color.highlight(self.period * 2)} (period * 2) values left
        4. Add the {Color.highlight("first half")} of the remaining numbers to the list of {Color.highlight("row values")},
           the {Color.highlight("second half")} to the list of {Color.highlight("column values")}
      
      {self.format_labeled(self.period, label="period")}
      {self.format_labeled(self.format_list(self.input_indexes, prefix=""), label="coords")}
      """
      Cipher.tutor(self, action="Split the list")
      # Finally, interpret the lists
      self.tutor_string = f"""
      The two lists can now be read directly as row/column values for the result.

      {self.format_labeled(self.format_list(self.output_rows, prefix=""), label="rows")}
      {self.format_labeled(self.format_list(self.output_columns, prefix=""), label="columns")}
      """
    else:
      Cipher.tutor(self, action="Create the lists")
      self.tutor_string = f"""
      With the lists of row and column values, we need to unify them into one list:
        1. Add the {Color.highlight(f"first {self.period}")} (current period value) row values to the full list
        2. Add the {Color.highlight(f"first {self.period}")} column values to the full list
        3. Repeat until all values have been added to the full list
      """
      Cipher.tutor(self, action="Create the full list")
      # Finally, interpret the full list
      self.tutor_string = f"""
      The full list that we now have is the list of row and column values for the result.
      For example, the first result character is at row={self.output_indexes[0]}, col={self.output_indexes[1]}

      {self.format_labeled(self.format_list(self.output_indexes, prefix=""), label="coords")}
      """
    # Final parent tutor call to solve the cipher
    Cipher.tutor(self)
  
  # This is a rare cipher where we need to add functionality to run()
  def run(self) -> None:
    """Run 'make_lists()' then perform inherited run method"""
    # Need to make lists before the tutorial runs
    self.make_lists()
    # Then we can do the normal run() functionality
    return super().run()

  # Simple process method since most of the logic is done by make_lists() before the tutor runs
  def process(self) -> None:
    # All we really need to do in here is read the lists created by make_lists()
    # and put spaces/special characters back in.
    index = 0
    self.result = ""
    # Loop through original string to handle spaces/special chars
    for char in self.input:
      new_char = ""
      # If it's an alphabetic character, add substituted character to result
      if char.isalpha():
        # Get index values from lists
        new_row = self.output_rows[index]
        new_col = self.output_columns[index]
        # Incremend index
        index += 1
        # Get new character from 2d indexes
        new_char = self.get_square_item(new_row, new_col)
        Logger.stepwise(self.format_change(char, new_char, suffix=f"(r={new_row}, c={new_col})"))
      else:
        # If not alphabetic, add it directly to the result
        new_char = char
        if char == "\n":
          Logger.stepwise("  newline\n")
        else:
          Logger.stepwise(self.format_change(char, new_char))
      # Add new char to result
      self.result += new_char
    # Extra stepwise newline for spacing
    Logger.stepwise()


class Random(Caesar):
  """Random substitution class
  
  This will perform a random monoalphabetic substitution on the input string.

  Extended methods:
    - __init__()
    - __str__()
    - alphabet_init()
    - tutor()
  
  Instance attributes:
    - randomize_hint (Cipher): a Cipher class indicating how to do the randomization 
                              (so far only Caesar is interpreted)
  
  String representation:
    - "random monoalphabetic substitution DESCRIPTION"
  """
  # This normally has a lot of similarities with the mixed alphabet keyword cipher,
  # but much of that cipher's logic revolves around handling a key that doesn't contain all letters.
  # In this situation, we also have a key but it has all letters in the alphabet
  # so a translation table can be immediately made from the key once generated.
  def __init__(self, input: str, randomize_hint: Cipher=Cipher) -> None:
    """Save provided randomize hint (a Cipher class) and input string"""
    self.randomize_hint = randomize_hint
    super().__init__(input)

  def __str__(self) -> str:
    return f"random monoalphabetic substitution ({self.summary})"
  
  def alphabet_init(self) -> None:
    """Generate randomized substitution table"""
    if self.randomize_hint is Caesar:
      Logger.tutorial("Generating random Caesar offset")
      self.offset = random.randint(1,25)
      self.direction = Caesar.Direction.RIGHT
      # Set summary for string method
      self.summary = f"Caesar {self.direction} {self.offset}"
      # Perform inherited initialization
      super().alphabet_init()
    else:
      # Perform general initialization
      super().alphabet_init()
      Logger.tutorial("Generating random monoalphabetic substitution")
      # Duplicate and shuffle the normal alphabet list
      random_list = self.alpha_list.copy()
      random.shuffle(random_list)
      # Initialize string for substitution alphabet
      self.lower_translation = ""

      # Loop through regular alphabet to check for non-substituted letters.
      # The shuffle() function doesn't guarantee that every item will move from its original position.
      # However, for our purposes, we would like each alphabetic letter 
      # to be substituted for a different letter.
      # This will accomplish that with one rare limitation:
      # if 'z' is not moved this will not be able to correct it.
      for char in self.alpha_list:
        # Substitution character should be the first item in the
        # remaining randomized alphabet array
        new_char = random_list[0]
        # If the substitution letter is the same and there are enough available random letters,
        if char == new_char and len(random_list) > 1:
          Logger.stepwise(f"Same letter ({char}), looking ahead")
          # use the next item in the array as the substitution letter instead
          new_char = random_list[1]
        # Add the substitution letter to the substitution alphabet string
        self.lower_translation += new_char
        Logger.stepwise(self.format_change(char, new_char))
        # Remove the substitution character from the array
        random_list.remove(new_char)
      
      Logger.tutorial(f"Finalized substitution alphabet: {self.lower_translation}")
      # Create uppercase copy and translation table
      self.upper_translation = self.lower_translation.upper()
      self.table = str.maketrans(self.lower_letters + self.upper_letters,
                                  self.lower_translation + self.upper_translation)
      # Set summary for string method
      self.summary = self.lower_translation
  
  def tutor(self, input=None, action=None) -> None:
    """There is no tutorial for random substitution"""
    pass


class Substitution(Monoalphabetic):
  """Custom substitution class

  Extended methods:
    - __init__()
    - __str__()
    - tutor()
    - alphabet_init()
    
  Additional methods:
    - process_args()

  Instance attributes:
    - sub_args (Iterable): list of substitution directives that will be interpreted by 'process_args()'
    - sub_case (Cipher.Case): case to use for substituted characters (default: 'Cipher.Case.UPPER')
  
  String representation
    - "Substitution from 'REGULAR_LETTERS' to 'SUBSTITUTION_LETTERS'
  """
  def __init__(self, input: str, sub_args: Iterable) -> None:
    """Store and process provided substitution arguments, set default substitution case and input string."""
    # This will go through each element in substitution_args and split by spaces
    self.sub_args = [item for sublist in (string.split() for string in sub_args) for item in sublist]
    self.sub_case = Cipher.Case.UPPER
    self.process_args()
    super().__init__(input)

  def __str__(self) -> str:
    return f"Substitution from '{self.lower_letters}' to '{self.lower_subs}'"

  def tutor(self) -> None:
    """There is no tutorial for custom substitution"""
    pass

  def process_args(self) -> None:
    """Process substitution directives from 'sub_args'.
    Details on the project wiki: https://gitlab.com/SirDoctorK/cipher/-/wikis/Ciphers/Custom-substitution
    """
    self.lower_subs = ""
    self.lower_letters = ""
    for arg in self.sub_args:
      if re.search('^file=', arg):
        # Grab filename as portion of argument after =
        file_name = arg.split('=')[1]
        Logger.tutorial(f"Reading directives from {file_name}")
        # Try-except in case the filename is invalid or unreadable
        try:
          infile = open(file_name, "r")
        except:
          infile = ""
          Logger.error(f"Couldn't open file {file_name}")
        # Add each line in the file as a new argument to process
        for line in infile:
          self.sub_args.append(line.rstrip())
      elif re.search('^case=', arg):
        # Grab case designator from argument and canonicalize
        try:
          self.sub_case = Cipher.Case.canonicalize(arg.split('=')[1])
        except ValueError as error:
          Logger.error(error)
          Logger.error("Will use default case")
      # Grab specified src and dst directives
      elif re.search('^(src|from)=', arg):
        newsrc = arg.split('=')[1]
        self.lower_letters += newsrc
      elif re.search('^(dst|to)=', arg):
        newdst = arg.split('=')[1]
        self.lower_subs += newdst
      # Verify that the first two characters are alphabetical
      elif arg[0].isalpha() and arg[1].isalpha():
        Logger.tutorial(f"Will substitute {arg}")
        self.lower_letters += arg[0]
        self.lower_subs += arg[1]
      else:
        Logger.error(f"Ignoring unintelligible argument: {arg}")
  
  def alphabet_init(self) -> None:
    """Construct substitution alphabet from the lists constructed in 'process_args()'"""
    # For this alphabet init, we assume that process_args has already run
    # and that the substitution strings are properly populated.
    if len(self.lower_letters) != len(self.lower_subs):
      Logger.error("Source and destination sets are not the same length;")
      Logger.error("will truncate to the shorter length")
      self.lower_letters = self.lower_letters[0:len(self.lower_subs)]
      self.lower_subs = self.lower_subs[0:len(self.lower_letters)]
    # List of tuples for substitution directives
    # First is the *substitution* character, that is, the plaintext letters that have been deciphered
    # That way it can be easily sorted to show which target substitution letters have not been used
    sortpairs = sorted(zip([*self.lower_subs], [*self.lower_letters]))
    # This gets each list back into the correct string
    self.lower_subs, self.lower_letters = [ ''.join(list(tuple)) for tuple in zip(*sortpairs) ]
    # Create uppercase copy of letter lists
    self.upper_letters = self.lower_letters.upper()
    self.upper_subs = self.lower_subs.upper()
    # In all situations, original letters should have both cases.
    # All substitution varies by requested cases and is handled in the if-then block.
    self.all_letters = self.lower_letters + self.upper_letters
    # Create table depending on substitution case
    if self.sub_case == Cipher.Case.ORIGINAL or self.sub_case == Cipher.Case.NONE:
      # ORIGINAL case means keep original case
      Logger.tutorial("Preserving original case")
      self.all_subs = self.lower_subs + self.upper_subs
    elif self.sub_case == Cipher.Case.LOWER:
      # LOWER puts the input in uppercase, then makes all replacements lowercase
      Logger.tutorial("Replacements will be in lowercase")
      self.input = self.input.upper()
      self.all_subs = self.lower_subs + self.lower_subs
    else:
      # UPPER (default) puts the input in lowercase, then makes all replacements uppercase
      Logger.tutorial("Replacements will be in uppercase")
      self.input = self.input.lower()
      self.all_subs = self.upper_subs + self.upper_subs
    # Create table
    self.table = str.maketrans(self.all_letters, self.all_subs)


########## Functions


def make_cipher_action(cipher_class, second_element) -> argparse.Action:
  """Generate an argparse action to use to populate a list of cipher operations

  The resulting action will append a tuple (cipher_class, second_element) to the set destination.

  Args:
    - cipher_class (Cipher): a class representing the cipher operation requested
    - second_element (any): the second element to the tuple
  
  If 'second_element' is "input", the resulting action will inherit from '_AppendAction'
  and use user input to the argument as the second element in the tuple.

  If 'second_element' is anything else, the resulting action class will inherit from '_AppendConstAction'
  and use the provided 'second_element' as the constant second element in the tuple.

  Returns:
    - argparse.Action: an argparse action that can be used to process a cipher flag
  """
  if second_element == "input":
    class AppendCipherAction(argparse._AppendAction):
      def __call__(self, parser: ArgumentParser, namespace: Namespace, values, option_string=None) -> None:
        values = (cipher_class, values)
        return super().__call__(parser, namespace, values, option_string)
  else:
    class AppendCipherAction(argparse._AppendConstAction):
      def __init__(self, option_strings, dest: str, const=(cipher_class, second_element), default=None, required=False, help=None, metavar=None) -> None:
        super().__init__(option_strings, dest, const, default, required, help, metavar)
  return AppendCipherAction


def make_store_enum(enum_class) -> argparse.Action:
  """Generate an argparse action to canonicalize and store the specified SmartEnum

  Args:
    - enum_class (SmartEnum): the enum class to canonicalize (must have a 'canonicalize()' method)

  Returns:
    - argparse.Action: the new argparse action
  """
  class StoreSmartEnum(argparse._StoreAction):
    def __call__(self, parser: ArgumentParser, namespace: Namespace, values, option_string = None) -> None:
      processed_value = enum_class.canonicalize(values)
      return super().__call__(parser, namespace, processed_value, option_string)
  return StoreSmartEnum


def get_args(custom_args=None) -> argparse.Namespace:
  """Get args with argparse and return the resulting namespace

  Args:
    - custom_args (list, optional): list of args to parse and store in namespace. Defaults to None (will interpret from cli arguments).

  Returns:
    - argparse.Namespace: the namespace resulting from 'parse_args()'
  """
  # Initialize parser 
  # No built-in help so we can add it to the "Script modifiers" group
  # Using alternate formatter so the longer description can be used
  # Setting description and epilog for usage synopsis
  parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.RawTextHelpFormatter, 
  description='''
  Cipher script
  Written by Stephen Kendall

  Capable of deciphering and enciphering with 
    Caesar, Atbash, A1Z26, Vigenere, Beaufort, Keyword, Playfair, and Bifid.
  The Vigenere implementation supports the Beaufort, Gronsfeld, and Autokey variations.
  If multiple ciphers are requested, the same input string will be processed by each one separately
    unless sequential operation is selected.''', 
  epilog="Consult the Wiki for more details: https://gitlab.com/SirDoctorK/cipher/-/wikis/home")

  ### Script modifiers
  # Default action for arguments is "store", defined explicitly for readability
  smod = parser.add_argument_group("Script Modifiers")
  smod.add_argument("-h", "--help", action="help", 
      help="show this help message and exit")
  smod.add_argument("-i", "--input", dest="file", action="store", 
      help="file containing text to process \nif this flag is not set, it will read from standard input through redirection or keyboard input")
  smod.add_argument("-g", "--generate", dest="gen", action="store_true",
      help=f"Generate input text from '{GENERATION_URL}'")
  smod.add_argument("-s", "--strip", "--strip-spaces", dest="strip_spaces", action="store_true", 
      help="strip space characters from output string")
  smod.add_argument("-v", "--verbose", action="count", default=0, 
      help="enable more detailed output (-v=info, -vv=tutorial, -vvv=step-by-step)")
  smod.add_argument("--runtime", action="store_true", 
      help="print detailed execution times (for debug)")
  smod.add_argument("-m", "--policy", "--mismatch-policy", action=make_store_enum(Cipher.InputPolicy),
      help="select policy for ciphers that don't operate on the contents of the input: warn (default), continue, or skip")
  smod.add_argument("-c", "--case", action=make_store_enum(Cipher.Case), 
      help="convert output string to the specified case")
    
  ### Cipher selectors
  ciphers = parser.add_argument_group("Cipher Selectors (at least one required)")
  ciphers.add_argument("-C", "--caesar", dest="ciphers", action=make_cipher_action(Caesar, 3), 
    help="use the Caesar cipher with a 3-character shift")
  ciphers.add_argument("-O", "--offset", type=int, metavar="DIST", dest="ciphers", action=make_cipher_action(Caesar, "input"), 
      help="use the Caesar cipher with the specified distance, inplies '-C'")
  ciphers.add_argument("-A", "--atbash", dest="ciphers", action=make_cipher_action(Atbash, None), 
      help="use the Atbash cipher")
  ciphers.add_argument("-Z", "--a1z26", dest="ciphers", action=make_cipher_action(A1Z26, None), 
      help="use the A1Z26 cipher")
  ciphers.add_argument("-V", "-G", "--vigenere", "--gronsfeld", metavar="KEY", dest="ciphers", action=make_cipher_action(Vigenere, "input"), 
      help="use the Vigenere cipher or a variant")
  ciphers.add_argument("-U", "--autokey", metavar="KEY", dest="ciphers", action=make_cipher_action(Autokey, "input"), 
      help="use the Vigenere Autokey cipher")
  ciphers.add_argument("-B", "--beaufort", metavar="KEY", dest="ciphers", action=make_cipher_action(Beaufort, "input"), 
      help="use the Beaufort cipher")
  ciphers.add_argument("-K", "--keyword", metavar="KEY", dest="ciphers", action=make_cipher_action(Keyword, "input"), 
      help="use the Keyword cipher")
  ciphers.add_argument("-P", "--polybius", metavar="KEY", dest="ciphers", action=make_cipher_action(Polybius, "input"), 
      help="use the Polybius cipher")
  ciphers.add_argument("-F", "--playfair", metavar="KEY", dest="ciphers", action=make_cipher_action(Playfair, "input"), 
      help="use the Playfair cipher")
  ciphers.add_argument("-I", "--bifid", metavar="KEY", dest="ciphers", action=make_cipher_action(Bifid, "input"), 
      help="use the Bifid cipher")
  ciphers.add_argument("-S", "--substitute", "--substitution", metavar="SUB",  dest="ciphers", action=make_cipher_action(Substitution, "input"), nargs="+",
      help="provide specific monoalphabetic substitutions to perform")
  ciphers.add_argument("-R", "--random", dest="ciphers", action=make_cipher_action(Random, None),
      help="randomly generate and apply a substitution alphabet")

  ### Cipher modifiers
  cmod = parser.add_argument_group("Cipher Modifiers")

  cmod.add_argument("-q", "--seq", "--sequential", action="store_true", 
      help="perform all requested cipher operations in sequential order, where the output of one goes into the next")
  cmod.add_argument("-b", "--brute", "--brute-force", action="store_true", 
      help="modify Caesar to print every shift up to DIST (default 3)")
  cmod.add_argument("-p", "--period", action="store", type=int, default=5, 
      help="specify the period for Bifid to intersperse y- and x-values (deafult %(default)s)")
  cmod.add_argument("-n", "--separator", "--number-separator", action="store", default="-",
      help="separator to use between numbers in numbered ciphers (default '%(default)s')")

  # Adding exclusive groups for -r/l and -e/d
  direction = cmod.add_mutually_exclusive_group()
  direction.add_argument("-r", "--right", dest="direction", action="store_const", const=Caesar.Direction.RIGHT, 
      help="use a right shift for Caesar and Vigenere ciphers (default)")
  direction.add_argument("-l", "--left", dest="direction", action="store_const", const=Caesar.Direction.LEFT, 
      help="use a left shift for Caesar and Vigenere ciphers")

  operation = cmod.add_mutually_exclusive_group()
  operation.add_argument("-e", "--encipher", dest="operation", action="store_const", const=Cipher.Operation.ENCIPHER, default=Cipher.Operation.ENCIPHER, 
      help="encipher input text (default)")
  operation.add_argument("-d", "--decipher", dest="operation", action="store_const", const=Cipher.Operation.DECIPHER, 
      help="decipher input text")
  ### All arguments set up

  # Send parsed args back to caller
  return parser.parse_args(custom_args)


def process_cipher_list(ciphers: list, target=Random) -> list:
  """Look for targets in list and return processed list
  
  The intent with this function is to congregate random cipher operations
  with the hints for the random operation (so far just Caesar)
  and to prevent other cipher operations from running when a random substitution is requested.

  If no targets found in the list: 
    - return the original list
  If at least one target in the list:
    - create a new list with one element for each target
    - look for non-target elements
    - if adjacent to a target
      - put it as the second element in that target's tuple

  Args:
    - ciphers (list): list of cipher operations requested
    - target (class, optional): class of operation to look for. Defaults to Random.

  Returns:
    - list: processed list of cipher operations
  """
  def compare_ciphers(cipher1, cipher2, target_list: list) -> type:
    """Compare ciphers and return the remainder

    Args:
      - cipher1 (type): type of the first cipher
      - cipher2 (type): type of the second cipher
      - target_list (list): new list of tuples created by parent function

    Returns:
      - type: remaining type that has not been used (returns NoneType if both were used)
    """
    if issubclass(cipher1, target):
      # Match [Random, Random] (consume first, leave second until we know the next item)
      if issubclass(cipher2, target):
        target_list.append((target, None))
        return cipher2
      # Match [Random, Cipher] (consume both)
      elif issubclass(cipher2, Cipher):
        target_list.append((target, cipher2))
        return type(None)
    elif issubclass(cipher1, Cipher):
      # Match [Cipher, Random] (consume both)
      if issubclass(cipher2, target):
        target_list.append((target, cipher1))
        return type(None)
      # Match [Cipher, Cipher] (skip first)
      else:
        return cipher2
    else:
      return cipher2
  # Start new list
  random_ciphers = []
  # Treat initial previous cipher as NoneType
  previous_cipher = type(None)
  for cipher, param in ciphers:
    # Compare and update previous cipher
    previous_cipher = compare_ciphers(previous_cipher, cipher, random_ciphers)
  # Extra compare to handle trailing Random operations
  compare_ciphers(previous_cipher, type(None), random_ciphers)
  # Return random_ciphers if non-empty, otherwise ciphers
  return random_ciphers or ciphers


def process_args(args: argparse.Namespace) -> argparse.Namespace:
  """Process parsed arguments and store in necessary variables

  Args:
    - args (argparse.Namespace): parsed args from 'get_args()'

  Raises:
    - ValueError: if no cipher operations were found in the list of arguments

  Returns:
    - argparse.Namespace: modified namespace with random cipher parsing and proper direction set
  """
  RunTime.enabled = args.runtime

  if args.policy:
    Cipher.policy = Cipher.InputPolicy.canonicalize(args.policy)
  
  Logger.set_print_level(args.verbose)
  Cipher.should_remove_spaces = args.strip_spaces

  # Set default direction
  # Could be done with argparse, but we also need en/de to influence direction
  # if direction is not explicitly specified
  if args.direction is None:
    if args.operation == Cipher.Operation.DECIPHER:
      args.direction = Caesar.Direction.LEFT
    else:
      args.direction = Caesar.Direction.RIGHT

  # Process case designator into canonical form
  # if args.case:
  #   Cipher.set_case(args.case)
  Cipher.output_case = args.case

  NumberedCipher.separator = args.separator
  
  if not args.ciphers:
    raise ValueError(f"No operation specified.\nUse at least one of the Cipher selector flags (see '{Color.highlight('cipher.py -h')}' for details)")
  # Process cipher list for randomizers
  args.ciphers = process_cipher_list(args.ciphers)
  Logger.tutorial(f"Cipher operations: {args.ciphers}")
  
  return args


def generate_str(gen_url=GENERATION_URL, target_div_id=GENERATION_DIV_ID) -> str:
  """Generate input string by fetching from website

  Args:
    - gen_url (str, optional): URL to use for random input string. Defaults to GENERATION_URL.
    - target_div_id (str, optional): ID of the div containing the desired content. Defaults to GENERATION_DIV_ID.

  Returns:
    - str: the input string found from the provided URL
  """
  Logger.info(f"Fetching generated input text from '{gen_url}'")
  # Set user agent in header to avoid 403 forbidden
  hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11'}
  req = Request(gen_url, headers=hdr)
  # Fetch contents of the random paragraph page
  # with error handling suggested by Python docs
  try:
    response = urlopen(req)
  except URLError as e:
    input = ''
    if hasattr(e, 'reason'):
      Logger.error('We failed to reach the server.')
      Logger.error('Reason: ', e.reason)
    elif hasattr(e, 'code'):
      Logger.error('The server couldn\'t fulfill the request.')
      Logger.error('Error code: ', e.code)
  else:
    response_html = response.read()
    # Decode response content bytes into string
    response_str = response_html.decode('utf-8')
    # Grab the part of the string after the opening div bracket, 
    # then grab the part of the string before the closing div bracket
    input = response_str.split(f'<div id="{target_div_id}">', 1)[1].split("</div>", 1)[0]
  return input


def init_str(generate=False, input_file="", input_string="") -> str:
  """Initialize input string from the provided parameters and return the new string

  Args:
    - generate (bool, optional): get input string from 'generate_str()'. Defaults to False.
    - input_file (str, optional): read input string from provided file name. Defaults to "".
    - input_string (str, optional): initialize and return this exact input string. Defaults to "".
  
  If none of the above args are provided, will read from stdin.

  Raises:
    - ValueError: if no input string can be obtained

  Returns:
    - str: initialized input string
  """
  # If set to generate input text, fetch that here
  if generate:
    input = generate_str()
  # If there is a filename, read from it
  elif input_file:
    Logger.info(f"Reading from {Color.highlight(input_file)}")
    input = open(input_file, "r").read()
  elif input_string:
    input = input_string
  # If no filename, reads from stdin with same behavior as 'cat' in bash edition
  else:
    # String needs to be initialized before it can be appended to
    input = ''
    if platform.system() == 'Windows':
      Logger.info(f"No filename, reading from stdin ('{Color.highlight('Ctrl-Z, Enter')}' to detach if interactive)")
    else:
      Logger.info(f"No filename, reading from stdin ('{Color.highlight('Ctrl-D')}' to detach if interactive)")
    for line in iter(sys.stdin.readline, ''):
      input += line
  # Strip leading and trailing newlines from input string (bash does this automatically)
  input = input.strip()
  # Exit script if the input string is empty
  if input == '':
    raise ValueError("Empty input string; nothing to do.")
    #sys.exit(1)
  # Any verbosity causes the original input string to be printed
  Logger.info(f"  Input string:\n{input}")
  return input


########## End of function declarations


########## Start of execution


# Main logic when run as a script
def main(custom_args=None, input_string="") -> int:
  """Main logic of cipher script

  Args:
    - custom_args (list, optional): list of args to parse to control operation. Defaults to None (read from cli).
    - input_string (str, optional): input string to process. Defaults to "" (read from stdin or relevant args).

  Returns:
    - int: return code (0=success, 1=error)
  """
  full_run_time = RunTime("Whole script")

  # Initialize variables from provided arguments
  try:
    args = get_args(custom_args)
    args = process_args(args)
    Logger.tutorial(args.ciphers)
    # Initialize strings
    input = init_str(args.gen, args.file, input_string)
  except Exception as e:
    # Exceptions should be turned into a return value for the function 
    # so they don't mess up the Flask app ('cipherApp.py')
    Logger.error(f"{type(e).__name__}: {e}")
    return 1

  for cipher, param in args.ciphers:
    # Initialize this cipher depending on the cipher operation requested
    if issubclass(cipher, Vigenere):
      this_cipher = cipher(input, key=param, direction=args.direction, operation=args.operation)
    elif issubclass(cipher, Random):
      this_cipher = cipher(input, randomize_hint=param)
    elif issubclass(cipher, Substitution):
      this_cipher = cipher(input, sub_args=param)
    elif issubclass(cipher, Caesar):
      if args.brute:
        # Special handling for brute force mode
        max_offset = min(param, 25)
        for i in range(1, max_offset):
          this_cipher = Caesar(input, dist=i, direction=args.direction, suppress_tutorial=True)
          this_cipher.run()
      this_cipher = cipher(input, dist=param, direction=args.direction)
    elif issubclass(cipher, Bifid):
      this_cipher = cipher(input, key=param, period=args.period, operation=args.operation)
    elif issubclass(cipher, KeyedCipher):
      this_cipher = cipher(input, key=param, operation=args.operation)
    else: # Default cipher initialization
      this_cipher = cipher(input, operation=args.operation)
    this_cipher.run()
    # Account for sequential operations
    if args.seq:
      input = this_cipher.result
  
  full_run_time.end()
  return 0


# Only call main if run as script
if __name__ == "__main__":
  sys.exit(main())
