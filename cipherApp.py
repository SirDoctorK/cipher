#!/usr/bin/python3


from flask import Flask, render_template, request
import cipher, analyze, io, sys, re
from typing import Callable


# Do not log to file on web site
cipher.Logger.file_level = cipher.Logger.Level.DISABLED
cipher.Color.clear_codes()


app = Flask(__name__)


# Run main cipher script logic on custom args and input string
def run_cipher(custom_args, input: str) -> str:
    # Create objects to convert I/O streams to regular strings
    output = io.StringIO()
    outerr = io.StringIO()
    # Replace stdout and stderr to capture cipher script output
    sys.stdout = output
    sys.stderr = outerr
    # Run main function from cipher script
    cipher.main(custom_args, input)
    # Restore stdout and stderr to normal values
    sys.stdout = sys.__stdout__
    sys.stderr = sys.__stderr__
    # Return string values for stdout and stderr
    return output.getvalue(), outerr.getvalue()


# Run function, capturing stdout and stderr
def run_function(func: Callable, custom_args, input: str) -> tuple:
    # Create objects to convert I/O streams to regular strings
    output = io.StringIO()
    outerr = io.StringIO()
    # Replace stdout and stderr to capture cipher script output
    sys.stdout = output
    sys.stderr = outerr
    # Run the requested function
    func(custom_args, input)
    # Restore stdout and stderr to normal values
    sys.stdout = sys.__stdout__
    sys.stderr = sys.__stderr__
    # Return string values for stdout and stderr
    return output.getvalue(), outerr.getvalue()


# Convert form data into args for the script
def process_cipher_options(form_items) -> list:
    # These are the args that will be returned for the script
    options = []
    print(form_items)
    # These ciphers need a key provided with them
    keyed_ciphers = ["vigenere", "autokey", "beaufort", "keyword", "polybius", "playfair", "bifid", "substitute"]
    # These will temporarily track additional form information that is needed
    search_key = search_txt = flag_name = ""
    # Patterns matching cipher selectors and parameters in the HTML.
    # For selectors, we have a capturing group around the digit(s)
    # so that we can isolate it easily later on to identify the 'search_key'.
    selector_pattern = re.compile("cipher-selector(\d+)")
    parameter_pattern = re.compile("cipher-(key|offset)")
    # Loop through all form items
    for key, value in form_items:
        print(f"received {key} with {value}")
        # Look for selectors and parameters each script run
        selector_match = re.search(selector_pattern, key)
        parameter_match = re.search(parameter_pattern, key)
        # input_str is handled separately
        if key == "input_str":
            continue
        # 'search_key' contains the exact key that is necessary for a pending cipher operation
        elif key == search_key and flag_name and value:
            # Once we have the additional value, append flag and value to the list
            options.append(f"--{flag_name}")
            options.append(value)
            # Clear out tracking variables
            flag_name = search_key = search_txt = ""
        # This branch will be taken if the current form item is a cipher dropdown
        elif selector_match:
            # Isolate number from the selector
            selector_num = selector_match.group(1)
            print(selector_num)
            # Save the cipher name as the flag to be added to the list
            flag_name = value
            # The Caesar cipher needs an additional offset value
            if value == "caesar":
                search_txt = "cipher-offset"
                # The cipher will also use this flag which accepts an argument
                flag_name = "offset"
            # Keyed ciphers need an additional key
            elif value in keyed_ciphers:
                search_txt = "cipher-key"
            # If we are looking for some kind of additional information, set the exact key to look for
            if search_txt:
                search_key = search_txt + selector_num
                print("searching for " + search_key)
            # Otherwise, just append the cipher name as a flag
            else:
                search_key = ""
                options.append(f"--{value}")
        # All other values that are not cipher parameters are handled here
        elif value and not parameter_match:
            # Handles checkbox items
            if value == "on":
                options.append(f"--{key}")
            # Handles radio button items
            elif key == "direction" or key == "operation":
                options.append(f"--{value}")
            # Special handling for the verbosity dropdown
            elif key == "verbosity":
                try:
                    level = int(value)
                except:
                    continue
                for i in range(level):
                    options.append("--verbose")
            # Any other items are assumed to be flag, arg pairs
            else:
                options.append(f"--{key}")
                options.append(value)
    return options


def process_analyzer_options(form_items) -> list:
    # These are the args that will be returned for the script
    options = []
    print(form_items)
    show_list = False
    # Loop through all form items
    for key, value in form_items:
        print(f"received {key} with {value}")
        if key == "input_str":
            continue
        # Special handling for the verbosity dropdown
        elif key == "verbosity":
            try:
                level = int(value)
            except:
                continue
            for i in range(level):
                options.append("--verbose")
        elif key == "list":
            show_list = True
        elif key == "list-num":
            if show_list:
                options.append("--list")
                options.append(value)
        elif value == "on":
            options.append(f"--{key}")
        elif value:
            options.append(f"--{key}")
            options.append(value)
    return options


# This is the actual endpoint for the cipher app containing the form to provide input to the script
@app.route("/")
def index():
    return render_template("cipher.html")


# This is to be used by the JavaScript on the app to get results and add to the page
@app.route("/submit-cipher", methods=["POST"])
def submit_cipher():
    print("running submit-cipher endpoint")
    result = errors = ""
    options = process_cipher_options(request.form.items())
    print(options)
    input = request.form.get("input_str")
    result, errors = run_function(cipher.main, options, input)
    print("finished running cipher")
    return render_template("script-result.html", result_output=result, result_error=errors)


# Same for submitting the analyzer form
@app.route("/submit-analyzer", methods=["POST"])
def submit_analyzer():
    print("running submit-analyzer endpoint")
    result = errors = ""
    options = process_analyzer_options(request.form.items())
    print(options)
    input = request.form.get("input_str")
    result, errors = run_function(analyze.main, options, input)
    print("finished running analyzer")
    return render_template("script-result.html", result_output=result, result_error=errors)


if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8080, debug=True)
